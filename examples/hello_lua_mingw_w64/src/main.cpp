// Example taken from http://lua-users.org/lists/lua-l/2010-06/msg00153.html
#include <stdio.h>
extern "C" {
#include <lua5.1/lua.h>
#include <lua5.1/lualib.h>
#include <lua5.1/lauxlib.h>
}
int main(int argc, char *argv[]) {
  // Open lua
  lua_State *L = lua_open();

  // Load the libraries
  luaL_openlibs(L);

  // Execution of a lua string
  luaL_dostring(L, "print \"Yo dude\"");

  // Load a string and then execute it.
  luaL_loadstring(L, "io.write(\"I'm here too\\n\")");
  lua_pcall(L, 0, LUA_MULTRET, 0);

  // Load from a file and then execute
  if (luaL_loadfile(L, "hello.lua") == 0) {
    // File loaded call it
    lua_pcall(L, 0, LUA_MULTRET, 0);
  }

  // Close lua
  lua_close (L);

  return 0;
}
