#!/bin/bash

set -e

script_path=`readlink -f $0`
script_dir=`dirname $script_path`
cd "$script_dir"
sibs build --platform win64 && ./sibs-build/win64/debug/hello_lua.exe
