const warn = @import("std").debug.warn;

extern fn baz() void;

export fn foo() void {
    warn("zig called from c!\n");
    baz();
}
