#include <stdio.h>
#include <zig/src/foo.h>

void baz()
{
    printf("c called from zig!\n");
}

int main(int argc, char **argv)
{
    printf("inside c\n");
    foo();
    return 0;
}
