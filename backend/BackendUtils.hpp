#pragma once

#include "../include/Conf.hpp"

namespace backend
{
    class Ninja;

    enum class RuntimeCompilerType
    {
        NONE,
        OTHER,
        CLANG,
        EMSCRIPTEN
    };
    
    class BackendUtils
    {
    public:
        static sibs::FileString getFileExtension(const sibs::FileString &filepath);
        static sibs::Language getFileLanguage(const _tinydir_char_t *extension);
        static sibs::Language getFileLanguage(tinydir_file *file);
        static void collectSourceFiles(const _tinydir_char_t *projectPath, Ninja *ninjaProject, const sibs::SibsConfig &sibsConfig, bool recursive = true);
        static std::vector<sibs::FileString> getCompilerCExecutable(sibs::Compiler compiler);
        static std::vector<sibs::FileString> getCompilerCppExecutable(sibs::Compiler compiler);
        static std::vector<sibs::FileString> getCompilerLinker(sibs::Compiler compiler);
        static RuntimeCompilerType getCCompilerType(sibs::Compiler compiler);
        static RuntimeCompilerType getCppCompilerType(sibs::Compiler compiler);
    };
}
