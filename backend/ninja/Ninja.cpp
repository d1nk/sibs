#include <cstring>
#include "../BackendUtils.hpp"
#include "Ninja.hpp"
#include "../../include/FileUtil.hpp"
#include "../../include/Exec.hpp"
#include "../../include/PkgConfig.hpp"
#include "../../include/GlobalLib.hpp"
#include "../../include/CmakeModule.hpp"
#include <algorithm>

using namespace std;
using namespace sibs;

#if OS_FAMILY == OS_FAMILY_POSIX
#include <unistd.h>
#define nprintf printf
#else
#define nprintf wprintf
#endif

namespace backend
{
    static std::string escape(const std::string &str)
    {
        std::string result;
        result.reserve(str.size());
        for(char c : str)
        {
            if(c == '\n')
                result += "$\n";
            else if(c == '$')
                result += "$$";
            else if(c == ' ')
                result += "$ ";
            else if(c == ':')
                result += "$:";
            else
                result += c;
        }
        return result;
    }

    static Ninja::LibraryType getNinjaLibraryType(PackageType packageType)
    {
        switch(packageType)
        {
            case PackageType::EXECUTABLE:
                return Ninja::LibraryType::EXECUTABLE;
            case PackageType::STATIC:
                return Ninja::LibraryType::STATIC;
            case PackageType::DYNAMIC:
            case PackageType::LIBRARY:
                return Ninja::LibraryType::DYNAMIC;
            default:
                assert(false);
                return (Ninja::LibraryType)-1;
        }
    }

    static string getIncludeOptionFlag(Compiler compiler, const string &filepath)
    {
        string result;
        switch (compiler)
        {
            case Compiler::MINGW_W64:
            case Compiler::GCC:
            {
                result = "'-I";
                result += filepath;
                result += "'";
                break;
            }
            case Compiler::MSVC:
            {
                result = "/I \"";
                result += filepath;
                result += "\"";
                break;
            }
            default:
                assert(false);
                break;
        }
        return result;
    }

    static string getDefineFlag(Compiler compiler, const string &name, const string &value)
    {
        switch (compiler)
        {
            case Compiler::MINGW_W64:
            case Compiler::GCC:
            {
                if(!value.empty())
                    return "'-D" + name + "=" + value + "'";
                else
                    return "'-D" + name + "'";
            }
            case Compiler::MSVC:
            {
                if(!value.empty())
                    return "\"/D" + name + "=" + value + "\"";
                else
                    return "\"/D" + name + "\"";
            }
            default:
                assert(false);
                break;
        }
        return "";
    }
    
    static vector<ninja::NinjaArg> getLanguageVersionFlag(Compiler compiler, CVersion cVersion)
    {
        switch (compiler)
        {
            case Compiler::MINGW_W64:
            case Compiler::GCC:
            {
                switch(cVersion)
                {
                    case CVersion::C89: return { ninja::NinjaArg("-std=c89"), ninja::NinjaArg("-pedantic") };
                    case CVersion::C99: return { ninja::NinjaArg("-std=c99"), ninja::NinjaArg("-pedantic") };
                    case CVersion::C11: return { ninja::NinjaArg("-std=c11"), ninja::NinjaArg("-pedantic") };
                    case CVersion::C20: return { ninja::NinjaArg("-std=c20"), ninja::NinjaArg("-pedantic") };
                }
                break;
            }
            case Compiler::MSVC:
            {
                // It's not possible to specify C version in MSVC, programmer will have to instead be careful to not use features that are outside
                // the desired version features...
                // MSVC specific extensions can be disabled by /Za /Ze but we dont want to do that, code could have check if compiler is MSVC
                // before using msvc extensions
                return {};
            }
        }
        assert(false);
        return {};
    }
    
    static vector<ninja::NinjaArg> getLanguageVersionFlag(Compiler compiler, CPPVersion cppVersion)
    {
        switch (compiler)
        {
            case Compiler::MINGW_W64:
            case Compiler::GCC:
            {
                switch(cppVersion)
                {
                    case CPPVersion::CPP03: return { ninja::NinjaArg("-std=c++03"), ninja::NinjaArg("-pedantic") };
                    case CPPVersion::CPP98: return { ninja::NinjaArg("-std=c++98"), ninja::NinjaArg("-pedantic") };
                    case CPPVersion::CPP11: return { ninja::NinjaArg("-std=c++11"), ninja::NinjaArg("-pedantic") };
                    case CPPVersion::CPP14: return { ninja::NinjaArg("-std=c++14"), ninja::NinjaArg("-pedantic") };
                    case CPPVersion::CPP17: return { ninja::NinjaArg("-std=c++17"), ninja::NinjaArg("-pedantic") };
                    case CPPVersion::CPP20: return { ninja::NinjaArg("-std=c++20"), ninja::NinjaArg("-pedantic") };
                }
                break;
            }
            case Compiler::MSVC:
            {
                switch(cppVersion)
                {
                    // Use /Za flag?
                    case CPPVersion::CPP03: return { ninja::NinjaArg("/std:c++03") };
                    case CPPVersion::CPP98: return { ninja::NinjaArg("/std:c++98") };
                    case CPPVersion::CPP11: return { ninja::NinjaArg("/std:c++11") };
                    case CPPVersion::CPP14: return { ninja::NinjaArg("/std:c++14") };
                    case CPPVersion::CPP17: return { ninja::NinjaArg("/std:c++17") };
                    case CPPVersion::CPP20: return { ninja::NinjaArg("/std:c++20") };
                }
                break;
            }
        }
        assert(false);
        return {};
    }

    static const char* getObjectFileExtension(Compiler compiler)
    {
        switch (compiler)
        {
            case Compiler::MINGW_W64:
            case Compiler::GCC:     return ".o";
            case Compiler::MSVC:    return ".obj";
            default:                return nullptr;
        }
    }

    Ninja::Ninja()
    {

    }
    
    void Ninja::addGlobalIncludeDirs(const string &globalIncludeDirs)
    {
        customGlobalIncludeDirs = globalIncludeDirs;
    }

    void Ninja::addSourceFile(sibs::Language language, const char *filepath)
    {
        string filePathStr = filepath ? filepath : "";
        if(filepath && !containsSourceFile(filePathStr))
            sourceFiles.push_back({ language, filePathStr });
    }

    void Ninja::setTestSourceDir(const char *dir)
    {
        string dirStr = dir ? dir : "";
        if(dir)
            testSourceDir = dirStr;
    }

    void Ninja::addDependency(const std::string &binaryFile)
    {
        if(!containsDependency(binaryFile))
            binaryDependencies.emplace_back(binaryFile);
    }

    void Ninja::addDynamicDependency(const std::string &dependency)
    {
        if(!containsDynamicDependency(dependency))
            dynamicDependencies.push_back(dependency);
    }
    
    void Ninja::addSubProject(Ninja *subProject, SibsConfig *config, sibs::FileString &&buildPath)
    {
        subProjects.emplace_back(NinjaSubProject{ subProject, config, move(buildPath) });
    }

    const std::vector<sibs::SourceFile>& Ninja::getSourceFiles() const
    {
        return sourceFiles;
    }

    bool Ninja::containsSourceFile(const string &filepath) const
    {
        for(const sibs::SourceFile &sourceFile : sourceFiles)
        {
            if(sourceFile.filepath == filepath)
                return true;
        }
        return false;
    }

    bool Ninja::containsDependency(const string &dependency) const
    {
        for(const string &binaryDependency : binaryDependencies)
        {
            if(binaryDependency == dependency)
                return true;
        }
        return false;
    }

    bool Ninja::containsDynamicDependency(const std::string &dependency) const
    {
        for(const string &dynamicDependency : dynamicDependencies)
        {
            if(dynamicDependency == dependency)
                return true;
        }
        return false;
    }

    Result<bool> Ninja::getLinkerFlags(const SibsConfig &config, LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc, LinkerFlagCallbackFunc dynamicLinkerFlagCallback, GlobalIncludeDirCallbackFunc globalIncludeDirCallback, CflagsCallbackFunc cflagsCallbackFunc) const
    {
        const vector<PackageListDependency> &packageListDependencies = config.getPackageListDependencies();
        
        Result<FileString> globalLibDirResult = getHomeDir();
        if (!globalLibDirResult)
            return Result<bool>::Err(globalLibDirResult);
        Path globalLibDir = Path(globalLibDirResult.unwrap()).join(".cache/sibs/lib").join(toFileString(asString(config.platform)));
        Result<bool> createGlobalLibDirResult = createDirectoryRecursive(globalLibDir.data.c_str());
        if(createGlobalLibDirResult.isErr())
            return createGlobalLibDirResult;

        // If pkg-config is not available then it will be ignored and we check if package instead exists as a global lib (on github etc)
        vector<PackageListDependency> globalLibDependencies;
        vector<PackageListDependency> pkgConfigDependencies;
        for(const PackageListDependency &dependency : packageListDependencies)
        {
            // PkgConfig libraries, even the static ones are most likely not built statically against libgcc/libc++, so we don't use them
            if(!config.packaging && PkgConfig::validatePkgConfigPackageVersionExists(dependency))
            {
                pkgConfigDependencies.push_back(dependency);
            }
            else
            {
                globalLibDependencies.push_back(dependency);
            }
        }

        Result<PkgConfigFlags> pkgConfigFlagsResult = PkgConfig::getDynamicLibsFlags(pkgConfigDependencies);
        if (!pkgConfigFlagsResult)
        {
            printf("%s, using global lib...\n", pkgConfigFlagsResult.getErrMsg().c_str());
            globalLibDependencies.reserve(globalLibDependencies.size() + pkgConfigDependencies.size());
            for (const PackageListDependency &pkgConfigDependency : pkgConfigDependencies)
            {
                globalLibDependencies.push_back(pkgConfigDependency);
            }
            pkgConfigDependencies.clear();
        }
        else
        {
            const PkgConfigFlags &pkgConfigFlag = pkgConfigFlagsResult.unwrap();
            if (!pkgConfigFlag.linkerFlags.empty())
                dynamicLinkerFlagCallback(pkgConfigFlag.linkerFlags);
            if(!pkgConfigFlag.cflags.empty())
                cflagsCallbackFunc(pkgConfigFlag.cflags);
        }

        return GlobalLib::getLibs(globalLibDependencies, config, globalLibDir.data, staticLinkerFlagCallbackFunc, dynamicLinkerFlagCallback, globalIncludeDirCallback);
    }
    
    Result<bool> Ninja::buildSubProjects(LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc, LinkerFlagCallbackFunc dynamicLinkerFlagCallback, GlobalIncludeDirCallbackFunc globalIncludeDirCallback)
    {
        for(auto &subProject : subProjects)
        {
            if(subProject.config->getPackageType() == PackageType::EXECUTABLE)
            {
                string errMsg = "The sub project ";
                errMsg += toUtf8(subProject.buildPath);
                errMsg += " is an executable. Only libraries can be sub projects";
                return Result<bool>::Err(errMsg);
            }

            if(subProject.config->shouldUseCmake())
            {
                CmakeModule cmakeModule;
                Result<bool> buildResult = cmakeModule.compile(*subProject.config, subProject.buildPath, staticLinkerFlagCallbackFunc, dynamicLinkerFlagCallback, globalIncludeDirCallback);
                if(!buildResult)
                    return buildResult;
            }
            else
            {
                Result<bool> buildResult = subProject.subProject->build(*subProject.config, subProject.buildPath.c_str(), staticLinkerFlagCallbackFunc, dynamicLinkerFlagCallback, globalIncludeDirCallback);
                if(!buildResult)
                    return buildResult;
            }
        }
        return Result<bool>::Ok(true);
    }

    static ninja::NinjaArg getCompilerSanitizerFlag(const SibsConfig &config)
    {
        if(config.getCompiler() == Compiler::GCC || config.getCompiler() == Compiler::MINGW_W64)
        {
            switch(config.getSanitize()) {
                case Sanitize::NONE:
                    return {};
                case Sanitize::ADDRESS:
                    return ninja::NinjaArg::createRaw("-fsanitize=address");
                case Sanitize::UNDEFINED:
                    return ninja::NinjaArg::createRaw("-fsanitize=undefined");
                case Sanitize::LEAK:
                    return ninja::NinjaArg::createRaw("-fsanitize=leak");
                case Sanitize::THREAD:
                    return ninja::NinjaArg::createRaw("-fsanitize=thread");
            }
        }
        return {};
    }

    static ninja::NinjaArg getlinkerSanitizerFlags(const SibsConfig &config) {
        switch(config.getSanitize()) {
            case Sanitize::NONE:
                return {};
            case Sanitize::ADDRESS:
                return ninja::NinjaArg::createRaw("-lasan");
            case Sanitize::UNDEFINED:
                return ninja::NinjaArg::createRaw("-lubsan");
            case Sanitize::LEAK:
                return ninja::NinjaArg::createRaw("-llsan");
            case Sanitize::THREAD:
                return ninja::NinjaArg::createRaw("-ltsan");
        }
        return {};
    }

    static vector<ninja::NinjaArg> getCompilerOptimizationFlags(const SibsConfig &config)
    {
        switch (config.getCompiler())
        {
            case Compiler::MINGW_W64:
            case Compiler::GCC:
            {
                switch (config.getOptimizationLevel())
                {
                    case OPT_LEV_DEBUG:
                    {
                        std::vector<ninja::NinjaArg> result = {
                            ninja::NinjaArg::createRaw("-O0 -g3"),
                            ninja::NinjaArg::createRaw("-D_GLIBCXX_ASSERTIONS -Wformat -Werror=format-security"),
                            ninja::NinjaArg::createRaw("-fasynchronous-unwind-tables"),
                            ninja::NinjaArg::createRaw("-D_DEBUG"),
                            ninja::NinjaArg::createRaw("-fno-omit-frame-pointer -fstack-clash-protection -fcf-protection"),
                        };
                        if(config.use_lto)
                            result.push_back(ninja::NinjaArg::createRaw("-flto"));
                        return result;
                    }
                    case OPT_LEV_RELEASE:
                    {
                        std::vector<ninja::NinjaArg> result = { ninja::NinjaArg::createRaw("-O3 -DNDEBUG") };
                        if(config.use_lto)
                            result.push_back(ninja::NinjaArg::createRaw("-flto"));
                        result.push_back(ninja::NinjaArg::createRaw(config.include_debug_symbols_in_release ? "-g3" : "-g0"));
                        return result;
                    }
                }
                break;
            }
            case Compiler::MSVC:
            {
                switch (config.getOptimizationLevel())
                {
                    case OPT_LEV_DEBUG:
                    {
                        return {
                            ninja::NinjaArg::createRaw("/Od"),
                            ninja::NinjaArg::createRaw("/Zi"),
                            ninja::NinjaArg::createRaw("/MTd"),
                            ninja::NinjaArg::createRaw("/D_DEBUG")
                        };
                    }
                    case OPT_LEV_RELEASE:
                    {
                        return {
                            ninja::NinjaArg::createRaw("/Ox"),
                            ninja::NinjaArg::createRaw("/MT"),
                            ninja::NinjaArg::createRaw("/DNDEBUG")
                        };
                    }
                }
                break;
            }
        }
        return {};
    }

    static string replaceAll(const string &input, const string &from, const string &to)
    {
        string result = input;
        size_t index = 0;
        while((index = result.find(from, index)) != string::npos)
        {
            result.replace(index, from.length(), to);
            index += to.length();
        }
        return result;
    }

    static vector<ninja::NinjaArg> extractIncludesFromCFlag(Compiler compiler, const string &cflag)
    {
        vector<ninja::NinjaArg> result;
        string includeStartStr;

        switch (compiler)
        {
            case Compiler::MINGW_W64:
            case Compiler::GCC:
            {
                includeStartStr = "-I";
                break;
            }
            case Compiler::MSVC:
            {
                includeStartStr = "/I";
                break;
            }
            default:
                assert(false);
                break;
        }

        size_t index = 0;
        while(index != string::npos)
        {
            char endChar = ' ';
            size_t includeStartIndex = cflag.find(includeStartStr, index);
            if(includeStartIndex == string::npos)
                break;
            else if(includeStartIndex > 0)
            {
                if(cflag[includeStartIndex - 1] == '"' || cflag[includeStartIndex - 1] == '\'')
                    endChar = cflag[includeStartIndex - 1];
            }
            includeStartIndex += includeStartStr.length();
            size_t includeEndIndex = cflag.find(endChar, includeStartIndex);
            index = includeEndIndex;

            if(includeEndIndex == string::npos)
                includeEndIndex = cflag.length();

            result.push_back(ninja::NinjaArg::createRaw(cflag.substr(includeStartIndex, includeEndIndex - includeStartIndex)));
        }

        return result;
    }

      static vector<ninja::NinjaArg> extractIncludesFromCFlags(Compiler compiler, const vector<ninja::NinjaArg> &cflags)
    {
        vector<ninja::NinjaArg> result;
        for(const ninja::NinjaArg &cflag : cflags)
        {
            vector<ninja::NinjaArg> subCFlags = extractIncludesFromCFlag(compiler, cflag.arg);
            result.insert(result.end(), subCFlags.begin(), subCFlags.end());
        }
        return result;
    }

    static string convertCIncludeSyntaxToZigInclude(Compiler compiler, const string &cIncludes)
    {
        vector<ninja::NinjaArg> result = extractIncludesFromCFlag(compiler, cIncludes);
        string resultStr;
        for(ninja::NinjaArg &include : result)
        {
            resultStr += " -isystem \"" + include.arg + "\"";
        }
        return resultStr;
    }

    static vector<ninja::NinjaArg> convertCFlagsIncludesToZigIncludes(Compiler compiler, const vector<ninja::NinjaArg> &cflags)
    {
        vector<ninja::NinjaArg> result = extractIncludesFromCFlags(compiler, cflags);
        for(ninja::NinjaArg &include : result)
        {
            include.arg = "-isystem \"" + include.arg + "\"";
        }
        return result;
    }

    static vector<ninja::NinjaArg> extractLibraryPathsWithoutFlags(Compiler compiler, const string &cLibraries)
    {
        vector<ninja::NinjaArg> result;

        size_t i = 0;
        while(i < cLibraries.size())
        {
            char c = cLibraries[i];
            if(c == '\'' || c == '"')
            {
                bool escapeQuote = false;
                char quoteSymbol = c;
                ++i;
                size_t libraryIndexStart = i;
                while(i < cLibraries.size())
                {
                    if(cLibraries[i] == '\\')
                        escapeQuote = !escapeQuote;
                    else if(cLibraries[i] == quoteSymbol && !escapeQuote)
                        break;
                    ++i;
                }
                size_t libraryIndexEnd = i;
                ++i;

                // This actually means the library path is malformed, but we can fix it here
                if(escapeQuote)
                    libraryIndexEnd--;
                
                if(strncmp(&cLibraries[libraryIndexStart], "-l", 2) == 0)
                    libraryIndexStart += 2;
                else if(strncmp(&cLibraries[libraryIndexStart], "-pthread", libraryIndexEnd - libraryIndexStart) == 0)
                    continue;

                size_t libraryPathLength = libraryIndexEnd - libraryIndexStart;
                if(libraryPathLength > 0)
                    result.push_back(ninja::NinjaArg::createRaw(cLibraries.substr(libraryIndexStart, libraryPathLength)));
            }
            else if(c != ' ')
            {
                bool escapeSpace = false;
                size_t libraryIndexStart = i;
                while(i < cLibraries.size() && !escapeSpace && cLibraries[i] != ' ')
                {
                    if(cLibraries[i] == '\\')
                        escapeSpace = !escapeSpace;
                    else if(cLibraries[i] == ' ' && !escapeSpace)
                        break;
                    ++i;
                }
                size_t libraryIndexEnd = i;
                ++i;

                // This actually means the library path is malformed, but we can fix it here
                if(escapeSpace)
                    libraryIndexEnd--;

                if(strncmp(&cLibraries[libraryIndexStart], "-l", 2) == 0)
                    libraryIndexStart += 2;
                else if(strncmp(&cLibraries[libraryIndexStart], "-pthread", libraryIndexEnd - libraryIndexStart) == 0)
                    continue;
                
                size_t libraryPathLength = libraryIndexEnd - libraryIndexStart;
                if(libraryPathLength > 0)
                    result.push_back(ninja::NinjaArg::createRaw(cLibraries.substr(libraryIndexStart, libraryPathLength)));
            }
            else
                ++i;
        }

        return result;
    }

    static vector<ninja::NinjaArg> convertCLibrariesToZigLibraries(Compiler compiler, const string &cLibraries)
    {
        vector<ninja::NinjaArg> result;
        result = extractLibraryPathsWithoutFlags(compiler, cLibraries);
        for(ninja::NinjaArg &include : result)
        {
            include.arg = "--library \"" + include.arg + "\"";
        }
        return result;
    }

    static int getFilenameLength(const string &filepath)
    {
        for(int i = filepath.size(); i >= 0; --i)
        {
            char c = filepath[i];
            if(c == '/' || c == '\\')
                return filepath.size() - i;
        }
        return filepath.size();
    }

    static string combineObjectFilesAsZigArgs(const vector<string> &objectFiles)
    {
        string result;
        for(const string &objectFile : objectFiles)
        {
            result += " --object " + escape(objectFile);
        }
        return result;
    }

#if OS_FAMILY == OS_FAMILY_WINDOWS
    struct PathSeperatorMatcher
    {
        bool operator()(char c) const
        {
            return c == '/' || c == '\\';
        }
    };
#else
    struct PathSeperatorMatcher
    {
        bool operator()(char c) const
        {
            return c == '/';
        }
    };
#endif

    static FileString getFileParentDirectory(const FileString &filepath)
    {
        auto it = find_if(filepath.rbegin(), filepath.rend(), PathSeperatorMatcher());
        if(it == filepath.rend())
            return TINYDIR_STRING("");
        else
            return filepath.substr(0, &*it.base() - &filepath[0]);
    }

    static string extractDynamicLibDirsFromLinkerFlag(const string &linkerFlag)
    {
        string result;

        FileString flagNative;
        if(linkerFlag.size() >= 2 && (linkerFlag[0] == '\'' || linkerFlag[0] == '"') && (linkerFlag.back() == '\'' || linkerFlag.back() == '"'))
            flagNative = toFileString(StringView(&linkerFlag[1], linkerFlag.size() - 2));
        else
            flagNative = toFileString(linkerFlag);
            
        if(getFileType(flagNative.c_str()) == FileType::REGULAR)
        {
            Result<FileString> libFullPath = getRealPath(flagNative.c_str());
            if(libFullPath)
            {
                FileString libDir = getFileParentDirectory(libFullPath.unwrap());
                if(!libDir.empty())
                {
                    result = toUtf8(libDir);
                }
            }
        }

        return result;
    }

    static string extractDynamicLibDirsFromLinkerFlags(const vector<string> &linkerFlags)
    {
        string result;
        for(const string &flag : linkerFlags)
        {
            string dynamicLibFullPath = extractDynamicLibDirsFromLinkerFlag(flag);
            if(!result.empty() && !dynamicLibFullPath.empty())
                result += ":";
            
            if(!dynamicLibFullPath.empty())
                result += dynamicLibFullPath;
        }
        return result;
    }

    static FileString array_to_file_string(const std::vector<FileString> &vec) {
        FileString result;
        for(const FileString &str : vec) {
            if(!result.empty())
                result += TINYDIR_STRING(" ");
            result += str;
        }
        return result;
    }

    Result<bool> Ninja::build(const SibsConfig &config, const _tinydir_char_t *savePath, LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc, LinkerFlagCallbackFunc dynamicLinkerFlagCallback, GlobalIncludeDirCallbackFunc globalIncludeDirCallback)
    {
        FileString cCompilerName = array_to_file_string(BackendUtils::getCompilerCExecutable(config.getCompiler()));
        FileString cppCompilerName = array_to_file_string(BackendUtils::getCompilerCppExecutable(config.getCompiler()));
        FileString compilerLinker = array_to_file_string(BackendUtils::getCompilerLinker(config.getCompiler()));

        RuntimeCompilerType cCompilerType = BackendUtils::getCCompilerType(config.getCompiler());
        RuntimeCompilerType cppCompilerType = BackendUtils::getCppCompilerType(config.getCompiler());

        if(cCompilerType != RuntimeCompilerType::NONE && cppCompilerType != RuntimeCompilerType::NONE && cCompilerType != cppCompilerType)
            return Result<bool>::Err("The c and c++ compiler has to be of the same type");

        RuntimeCompilerType compilerType = cCompilerType;
        if(compilerType == RuntimeCompilerType::NONE)
            compilerType = cppCompilerType;

        Sanitize sanitize = config.getSanitize();
        if(compilerType == RuntimeCompilerType::EMSCRIPTEN)
            sanitize = Sanitize::NONE;

        Result<bool> createBuildDirResult = createDirectoryRecursive(savePath);
        if (!createBuildDirResult)
            return createBuildDirResult;

        Path generatedHeadersDir = Path(config.getProjectPath()).join(TINYDIR_STRING("sibs-build")).join(toFileString(asString(config.platform))).join(TINYDIR_STRING("generated-headers"));
        Result<bool> createGeneratedHeadersDirResult = createDirectoryRecursive(generatedHeadersDir.data.c_str());
        if (!createGeneratedHeadersDirResult)
            return createGeneratedHeadersDirResult;
        
        Path generatedZigHeadersDir = Path(generatedHeadersDir).join(TINYDIR_STRING("zig"));
        Result<bool> createGeneratedZigHeadersDirResult = createDirectory(generatedZigHeadersDir.data.c_str());
        if (!createGeneratedZigHeadersDirResult)
            return createGeneratedZigHeadersDirResult;

        string generatedZigHeaderDirUtf8 = toUtf8(generatedZigHeadersDir.data);

        LibraryType libraryType = getNinjaLibraryType(config.getPackageType());
        // TODO: Instead of statically linking everything, maybe we should build everything as they prefer to be built
        // and then copy the files if they are shared libraries to the same directory as the root project executable
        // so they can be packaged into an archive that can be distributed?
        if(config.packaging && !config.isMainProject())
            libraryType = LibraryType::STATIC;

        // TODO: Emscripten dynamic library support is not good at the moment, remove this once emscripten has been improved
        if(libraryType == LibraryType::DYNAMIC && compilerType == RuntimeCompilerType::EMSCRIPTEN)
            libraryType = LibraryType::STATIC;

        string savePathUtf8 = toUtf8(savePath);
        string projectPathUtf8 = toUtf8(config.getProjectPath());

        Path ninjaBuildFilePath = Path(savePath).join(TINYDIR_STRING("build.ninja"));

        ninja::NinjaBuildFile ninjaBuildFile;

        string globalIncDir;
#if OS_FAMILY == OS_FAMILY_POSIX
        globalIncDir += "-I/usr/local/include";
#endif
        for(const auto &includeDir : config.getIncludeDirs())
        {
            globalIncDir += " ";
            if(isPathAbsolute(includeDir))
            {
                globalIncDir += getIncludeOptionFlag(config.getCompiler(), includeDir);
            }
            else
            {
                string includeDirRelative = "../../../";
                includeDirRelative += includeDir;
                globalIncDir += getIncludeOptionFlag(config.getCompiler(), includeDirRelative);
            }
        }

        auto parentGlobalIncludeDirCallback = globalIncludeDirCallback;
        for (const string &globalIncludeDir : config.getGlobalIncludeDirs())
        {
            string globalIncludeDirFull = projectPathUtf8;
            globalIncludeDirFull += "/";
            globalIncludeDirFull += globalIncludeDir;
            if(parentGlobalIncludeDirCallback)
                parentGlobalIncludeDirCallback(globalIncludeDirFull);
        }

        string dependencyExportIncludeDirs = customGlobalIncludeDirs;
        globalIncludeDirCallback = [&parentGlobalIncludeDirCallback, &dependencyExportIncludeDirs, &config](const string &globalIncludeDir)
        {
            dependencyExportIncludeDirs += " ";
            dependencyExportIncludeDirs += getIncludeOptionFlag(config.getCompiler(), globalIncludeDir);
            if (parentGlobalIncludeDirCallback)
                parentGlobalIncludeDirCallback(globalIncludeDir);
        };

        // TODO: Allow configuring default linking flags. Maybe have `package.useThreads = false` to disable this flag
        string allLinkerFlags;
        if(isSamePlatformFamily(config.platform, PLATFORM_LINUX))
            allLinkerFlags = "-pthread";

        // TODO: Somehow check loading order, because it has to be correct to work.. Or does it for dynamic libraries?
        // Anyways it's required for static libraries
        for (const string &binaryDependency : binaryDependencies)
        {
            allLinkerFlags += " ";
            allLinkerFlags += binaryDependency;
        }

        vector<string> linkerFlags;
        vector<string> dynamicLinkerFlags;
        
        auto parentProjStaticLinkerFlagCallbackFunc = staticLinkerFlagCallbackFunc;
        if (!staticLinkerFlagCallbackFunc || libraryType == LibraryType::DYNAMIC)
        {
            staticLinkerFlagCallbackFunc = [&linkerFlags](const string &linkerFlag)
            {
                linkerFlags.push_back(linkerFlag);
            };
        }

        auto parentProjDynamicLinkerFlagCallbackFunc = dynamicLinkerFlagCallback;
        if(!dynamicLinkerFlagCallback || libraryType != LibraryType::STATIC)
        {
            dynamicLinkerFlagCallback = [&linkerFlags, &dynamicLinkerFlags, &parentProjDynamicLinkerFlagCallbackFunc](const string &linkerFlag)
            {
                if(parentProjDynamicLinkerFlagCallbackFunc)
                    parentProjDynamicLinkerFlagCallbackFunc(linkerFlag);
                linkerFlags.push_back(linkerFlag);
                dynamicLinkerFlags.push_back(linkerFlag);
            };
        }

        for(const string &dynamicDependency : dynamicDependencies)
        {
            dynamicLinkerFlagCallback(dynamicDependency);
        }
        
        vector<ninja::NinjaArg> cflags = customCflags;
        auto cflagsCallbackFunc = [&cflags](const string &dependencyCflags)
        {
            cflags.push_back(ninja::NinjaArg::createRaw(dependencyCflags));
        };
        
        Result<bool> buildSubProjectResult = buildSubProjects(staticLinkerFlagCallbackFunc, dynamicLinkerFlagCallback, globalIncludeDirCallback);
        if(!buildSubProjectResult)
            return buildSubProjectResult;

        Result<bool> linkerFlagsResult = getLinkerFlags(config, staticLinkerFlagCallbackFunc, dynamicLinkerFlagCallback, globalIncludeDirCallback, cflagsCallbackFunc);
        if (linkerFlagsResult.isErr())
            return Result<bool>::Err(linkerFlagsResult.getErrMsg());
        
        for(vector<string>::reverse_iterator it = linkerFlags.rbegin(), end = linkerFlags.rend(); it != end; ++it)
        {
            allLinkerFlags += " ";
            allLinkerFlags += *it;
        }

        if(parentGlobalIncludeDirCallback)
        {
            vector<ninja::NinjaArg> cflagIncludes = extractIncludesFromCFlags(config.getCompiler(), cflags);
            for(const ninja::NinjaArg &include : cflagIncludes)
            {
                parentGlobalIncludeDirCallback(include.arg);
            }
        }

        globalIncDir += dependencyExportIncludeDirs;
        globalIncDir += " " + getIncludeOptionFlag(config.getCompiler(), toUtf8(generatedHeadersDir.data));
        ninjaBuildFile.defineGlobalVariable("globalIncDir", globalIncDir);
        // TODO: Find a better way to convert includes, this could be slow...
        ninjaBuildFile.defineGlobalVariable("globalIncDirZig", convertCIncludeSyntaxToZigInclude(config.getCompiler(), globalIncDir));

        vector<ninja::NinjaArg> defines;
        for(const auto &definePair : config.getDefines())
        {
            defines.push_back(ninja::NinjaArg::createRaw(getDefineFlag(config.getCompiler(), definePair.first, definePair.second)));
        }
        
        switch (config.platform)
        {
            case PLATFORM_WIN64:
            {
                for(const string &define : { "WIN64", "_WIN64", "WIN32", "_WIN32" })
                {
                    defines.push_back(ninja::NinjaArg::createRaw(getDefineFlag(config.getCompiler(), define, "")));
                }
                break;
            }
            case PLATFORM_WIN32:
            {
                for(const string &define : { "WIN32", "_WIN32" })
                {
                    defines.push_back(ninja::NinjaArg::createRaw(getDefineFlag(config.getCompiler(), define, "")));
                }
                break;
            }
            default:
                break;
        }
        
        if(SYSTEM_PLATFORM == PLATFORM_WIN32 || SYSTEM_PLATFORM == PLATFORM_WIN64)
        {
            switch(libraryType)
            {
                // TODO: Executable type does not guarantee the executable should be a console on windows. Find a way to define window type as well
                case LibraryType::EXECUTABLE:
                    defines.push_back(ninja::NinjaArg::createRaw(getDefineFlag(config.getCompiler(), "_CONSOLE", "")));
                    break;
                case LibraryType::STATIC:
                    defines.push_back(ninja::NinjaArg::createRaw(getDefineFlag(config.getCompiler(), "_LIB", "")));
                    break;
            }
        }

        // TODO: Verify compilers for language are installed when attempting to build a project that has source files of those types

        vector<ninja::NinjaArg> compileCCommand;
        vector<ninja::NinjaArg> compileCppCommand;

        switch(config.getCompiler())
        {
            case Compiler::MINGW_W64:
            case Compiler::GCC:
            {
                vector<ninja::NinjaArg> baseCompileCArgs;
                switch(libraryType)
                {
                    case LibraryType::EXECUTABLE:
                    {
                        baseCompileCArgs.insert(baseCompileCArgs.end(), { 
                            ninja::NinjaArg("-c"), 
                            ninja::NinjaArg::createRaw("-fPIE -fPIC"), 
                            ninja::NinjaArg("$in"), 
                            ninja::NinjaArg("-o"), 
                            ninja::NinjaArg("$out")
                        });
                        break;
                    }
                    case LibraryType::STATIC:
                    case LibraryType::DYNAMIC:
                    {
                        baseCompileCArgs.insert(baseCompileCArgs.end(), { 
                            ninja::NinjaArg("-c"), 
                            ninja::NinjaArg("-fPIC"),
                            ninja::NinjaArg("$in"), 
                            ninja::NinjaArg("-o"), 
                            ninja::NinjaArg("$out")
                        });
                        break;
                    }
                }

                compileCCommand.insert(compileCCommand.end(), baseCompileCArgs.begin(), baseCompileCArgs.end());
                compileCCommand.insert(compileCCommand.end(), { 
                    ninja::NinjaArg::createRaw("-fdiagnostics-show-option"),
                    ninja::NinjaArg::createRaw("-fdiagnostics-color=always"),
                    ninja::NinjaArg::createRaw("-pipe"),
                    ninja::NinjaArg::createRaw("-D_FILE_OFFSET_BITS=64"),
                    ninja::NinjaArg::createRaw("-Winvalid-pch -D_GNU_SOURCE -D_BSD_SOURCE -D_DEFAULT_SOURCE"),
                });

                if(config.getCompiler() != Compiler::MINGW_W64)
                    compileCCommand.push_back(ninja::NinjaArg::createRaw("-fstack-protector"));

                compileCCommand.insert(compileCCommand.end(), { 
                    ninja::NinjaArg::createRaw("-MMD -MF $out.d"),
                    ninja::NinjaArg::createRaw("$globalIncDir")
                });

                compileCCommand.insert(compileCCommand.end(), cflags.begin(), cflags.end());
                compileCCommand.insert(compileCCommand.end(), defines.begin(), defines.end());

                if(config.showWarnings)
                {
                    compileCCommand.insert(compileCCommand.end(), { 
                        ninja::NinjaArg("-Wall"), 
                        ninja::NinjaArg("-Wextra") 
                    });

                    if(!config.errorOnWarning)
                        compileCCommand.push_back(ninja::NinjaArg("-Werror=return-type"));
                }
                else
                {
                    compileCCommand.push_back(ninja::NinjaArg("-w"));
                }

                if(config.errorOnWarning)
                {
                    compileCCommand.push_back(ninja::NinjaArg("-Werror"));
                }

                vector<ninja::NinjaArg> optimizationFlags = getCompilerOptimizationFlags(config);
                compileCCommand.insert(compileCCommand.end(), optimizationFlags.begin(), optimizationFlags.end());

                ninja::NinjaArg sanitizerFlag = getCompilerSanitizerFlag(config);
                if(sanitizerFlag.type != ninja::NinjaArg::Type::NONE)
                    compileCCommand.push_back(std::move(sanitizerFlag));

                compileCppCommand = compileCCommand;
                compileCppCommand.insert(compileCppCommand.end(), { 
                    ninja::NinjaArg(config.enableExceptions ? "-fexceptions" : "-fno-exceptions"),
                    ninja::NinjaArg("-Wnon-virtual-dtor")
                });

                compileCCommand.insert(compileCCommand.begin(), ninja::NinjaArg::createRaw(cCompilerName));
                compileCppCommand.insert(compileCppCommand.begin(), ninja::NinjaArg::createRaw(cppCompilerName));
                break;
            }
            case Compiler::MSVC:
            {
                compileCCommand.insert(compileCCommand.end(), { 
                    ninja::NinjaArg::createRaw("cl.exe /showIncludes"),
                    ninja::NinjaArg::createRaw("/c"), 
                    ninja::NinjaArg::createRaw("$in"), 
                    ninja::NinjaArg::createRaw("/Fo$out"),
                    ninja::NinjaArg::createRaw("$globalIncDir"),
                    ninja::NinjaArg::createRaw("/FS")
                });

                compileCCommand.insert(compileCCommand.end(), cflags.begin(), cflags.end());
                compileCCommand.insert(compileCCommand.end(), defines.begin(), defines.end());

                if(config.showWarnings)
                    compileCCommand.push_back(ninja::NinjaArg("/Wall"));
                else
                    compileCCommand.push_back(ninja::NinjaArg("/w"));

                if(config.errorOnWarning)
                    compileCCommand.push_back(ninja::NinjaArg("/WX"));

// TODO: Remove this once locate_windows_sdk has been updated to locate multiple arch windows sdk
#if OS_TYPE == OS_TYPE_WINDOWS && defined(SIBS_ENV_32BIT)
#error "sibs is currently not supported on windows 32-bit because locate_windows_sdk can only locate x64 windows sdk"
#endif

                switch (SYSTEM_PLATFORM)
                {
                    case PLATFORM_WIN32:
                        compileCCommand.push_back(ninja::NinjaArg("/MACHINE:X86"));
                        break;
                    case PLATFORM_WIN64:
                        compileCCommand.push_back(ninja::NinjaArg("/MACHINE:X64"));
                        break;
                }

                vector<ninja::NinjaArg> optimizationFlags = getCompilerOptimizationFlags(config);
                compileCCommand.insert(compileCCommand.end(), optimizationFlags.begin(), optimizationFlags.end());

                ninja::NinjaArg sanitizerFlag = getCompilerSanitizerFlag(config);
                if(sanitizerFlag.type != ninja::NinjaArg::Type::NONE)
                    compileCCommand.push_back(std::move(sanitizerFlag));

                compileCppCommand = compileCCommand;
                compileCppCommand.push_back(ninja::NinjaArg("/EHs"));
                break;
            }
        }

        bool zigTest = (config.zigTestAllFiles || !config.zigTestFiles.empty());

        vector<ninja::NinjaArg> cLanguageVersionFlags = getLanguageVersionFlag(config.getCompiler(), config.getCversion());
        compileCCommand.insert(compileCCommand.end(), cLanguageVersionFlags.begin(), cLanguageVersionFlags.end());

        vector<ninja::NinjaArg> cppLanguageVersionFlags = getLanguageVersionFlag(config.getCompiler(), config.getCppVersion());
        compileCppCommand.insert(compileCppCommand.end(), cppLanguageVersionFlags.begin(), cppLanguageVersionFlags.end());

        ninja::NinjaRule *compileCRule = ninjaBuildFile.createRule("compile_c", compileCCommand);
        ninja::NinjaRule *compileCppRule = ninjaBuildFile.createRule("compile_cpp", compileCppCommand);
        
        switch(config.getCompiler())
        {
            case Compiler::MINGW_W64:
            case Compiler::GCC:
            {
                compileCRule->depFile = "$out.d";
                compileCppRule->depFile = "$out.d";
                break;
            }
            case Compiler::MSVC:
            {
                compileCRule->deps = "msvc";
                compileCppRule->deps = "msvc";
                break;
            }
        }

        bool crossOsCompiling = !isSamePlatformFamily(config.platform, SYSTEM_PLATFORM);

        // TODO: Specify -mconsole or -mwindows for windows.
        // TODO: Convert sibs defines to const variables in a zig file that other zig files can include (like a config file).

        // TODO: Remove --library c if project does not depend on c libraries or project only has .zig files

        vector<ninja::NinjaArg> commonZigArgs = {
            ninja::NinjaArg::createRaw("-isystem ../../../"),
            ninja::NinjaArg::createRaw("--color on"),
            ninja::NinjaArg::createRaw("$globalIncDirZig")
        };

        if(isSamePlatformFamily(SYSTEM_PLATFORM, PLATFORM_LINUX) && isSamePlatformFamily(config.platform, PLATFORM_WIN))
            commonZigArgs.push_back(ninja::NinjaArg::createRaw("--target-os windows --target-arch x86_64 --target-environ gnu --libc-lib-dir /usr/x86_64-w64-mingw32/lib --libc-static-lib-dir /usr/x86_64-w64-mingw32/lib"));

        // TODO: Remove this if project does not depend on c libraries or project only has .zig files
        if(!config.packaging)
            commonZigArgs.push_back(ninja::NinjaArg::createRaw("--library c"));

        ninja::NinjaVariable zigHeaderFile("headerFile");
        vector<ninja::NinjaArg> zigTestArgs = {
            ninja::NinjaArg::createRaw("zig test $in --name $out -femit-h $headerFile")
        };

        vector<ninja::NinjaArg> compileZigArgs = {
            ninja::NinjaArg::createRaw("zig build-obj"),
            ninja::NinjaArg::createRaw("$in"),
            ninja::NinjaArg::createRaw("-femit-h"),
            ninja::NinjaArg::createRaw("--name $out")
        };
        
        // TODO: Find a way to do this more efficiently
        vector<ninja::NinjaArg> cflagsIncludes = convertCFlagsIncludesToZigIncludes(config.getCompiler(), cflags);
        commonZigArgs.insert(commonZigArgs.end(), cflagsIncludes.begin(), cflagsIncludes.end());

        if(config.getOptimizationLevel() == sibs::OPT_LEV_RELEASE)
        {
            // TODO: Specify a way to change these options, either in project.conf or argument to sibs build
            commonZigArgs.push_back(ninja::NinjaArg::createRaw("--release-safe"));
            if(!config.include_debug_symbols_in_release)
                commonZigArgs.push_back(ninja::NinjaArg::createRaw("--strip"));
        }

        zigTestArgs.insert(zigTestArgs.end(), commonZigArgs.begin(), commonZigArgs.end());
        compileZigArgs.insert(compileZigArgs.end(), commonZigArgs.begin(), commonZigArgs.end());

        if(libraryType == LibraryType::STATIC)
            compileZigArgs.push_back(ninja::NinjaArg::createRaw("--static"));
        else if(libraryType == LibraryType::DYNAMIC) // TODO: Verify if this is the correct way to handle dynamic libraries in zig
            compileZigArgs.push_back(ninja::NinjaArg::createRaw("-rdynamic"));

        if(zigTest)
        {
            // TODO: Verify if we really need to add all libraries for every object file
            vector<ninja::NinjaArg> zigLibraryFlags = convertCLibrariesToZigLibraries(config.getCompiler(), allLinkerFlags);
            zigTestArgs.insert(zigTestArgs.end(), zigLibraryFlags.begin(), zigLibraryFlags.end());
        }

        ninja::NinjaRule *compileZigRule = ninjaBuildFile.createRule("compile_zig", compileZigArgs);
        ninja::NinjaRule *testZigRule = ninjaBuildFile.createRule("zig_test", zigTestArgs);

        bool usesCFiles = false;
        bool usesCppFiles = false;
        bool onlyZigFiles = true;

        vector<string> objectNames;
        objectNames.reserve(sourceFiles.size());
        vector<ninja::NinjaBuild*> zigBuilds;
        // TODO(Urgent): Instead of adding zig builds as dependencies for all c/c++ builds, generate two ninja files; one for zig and one for c/c++.
        // Then build run zig ninja file first and then c/c++ ninja file.
        // That would be more efficient as you dont need to add every single zig build as dependency for all c/c++ builds...
        for(const sibs::SourceFile &sourceFile : sourceFiles)
        {
            if(sourceFile.language == sibs::Language::ZIG)
            {
                string objectName = config.getPackageName() + "@exe/" + sourceFile.filepath;
                objectName += getObjectFileExtension(config.getCompiler());

                int filenameLength = getFilenameLength(sourceFile.filepath);
                string zigSourceDirRelative = sourceFile.filepath.substr(0, sourceFile.filepath.size() - filenameLength);
                FileString zigHeaderFileDir = toFileString(generatedZigHeaderDirUtf8 + '/' + zigSourceDirRelative);
                Result<bool> createZigHeaderFileDirResult = createDirectoryRecursive(zigHeaderFileDir.c_str());
                if(!createZigHeaderFileDirResult)
                    return createZigHeaderFileDirResult;
                
                string headerName = sourceFile.filepath.substr(sourceFile.filepath.size() - filenameLength, filenameLength - 4) + ".h";
                ninja::NinjaArgValue zigHeaderFileValue = { zigHeaderFile, '"' + toUtf8(zigHeaderFileDir) + '/' + headerName + '"' };
                ninja::NinjaBuild *ninjaBuild = nullptr;
                if(zigTest)
                    ninjaBuild = testZigRule->build(projectPathUtf8 + "/" + sourceFile.filepath, objectName, { zigHeaderFileValue });
                else
                    ninjaBuild = compileZigRule->build(projectPathUtf8 + "/" + sourceFile.filepath, objectName, { zigHeaderFileValue });
                ninjaBuild->alwaysRun = true;
                zigBuilds.push_back(ninjaBuild);

                objectNames.emplace_back(move(objectName));
            }
            else
                onlyZigFiles = false;
        }

        bool containsZigFiles = false;
        for(const sibs::SourceFile &sourceFile : sourceFiles)
        {
            string objectName;
            switch(sourceFile.language)
            {
                case sibs::Language::C:
                {
                    objectName += config.getPackageName() + "@exe/" + sourceFile.filepath;
                    objectName += getObjectFileExtension(config.getCompiler());
                    compileCRule->build(projectPathUtf8 + "/" + sourceFile.filepath, objectName, {}, zigBuilds);
                    usesCFiles = true;
                    break;
                }
                case sibs::Language::CPP:
                {
                    objectName += config.getPackageName() + "@exe/" + sourceFile.filepath;
                    objectName += getObjectFileExtension(config.getCompiler());
                    compileCppRule->build(projectPathUtf8 + "/" + sourceFile.filepath, objectName, {}, zigBuilds);
                    usesCppFiles = true;
                    break;
                }
                case sibs::Language::ZIG:
                {
                    // Already built above
                    containsZigFiles = true;
                    break;
                }
                default:
                    assert(false);
                    break;
            }

            if(!objectName.empty())
                objectNames.emplace_back(move(objectName));
        }

        if(containsZigFiles && isSamePlatformFamily(config.platform, PLATFORM_WIN) && !isSamePlatformFamily(SYSTEM_PLATFORM, PLATFORM_WIN))
            return Result<bool>::Err("Cross compiling a project with zig files from a non-windows platform to windows is currently not supported because zig doesn't support libc when cross compiling");

        string packagingFlags;
        if(config.packaging)
        {
            switch(config.getCompiler())
            {
                case Compiler::MINGW_W64:
                case Compiler::GCC:
                {
                    if(usesCFiles) {
                        packagingFlags += " -static-libgcc";
                    }
                    if(usesCppFiles) {
                        packagingFlags += " -static-libstdc++ -Wl,-Bstatic -lstdc++ -Wl,-Bdynamic";
                    }
                    break;
                }
                case Compiler::MSVC:
                {
                    // We always statically link using /MT so there is no need to do it here
                    break;
                }
            }
        }
        else
        {
            if(config.getCompiler() == Compiler::MINGW_W64)
            {
                if(usesCFiles) {
                    packagingFlags += " -static-libgcc";
                }
                if(usesCppFiles) {
                    packagingFlags += " -static-libstdc++ -Wl,-Bstatic -lstdc++ -Wl,-Bdynamic";
                }
            }
        }

        // TODO: For now zig projects (zig object files) are built with c/c++ compiler,
        // they should be built with zig if project only contains zig files.
        // But how to combine object files with zig? build-exe only wants to accept .zig files
        string projectGeneratedBinary;
        if (!sourceFiles.empty() && !zigTest)
        {
            projectGeneratedBinary = "\"";
            projectGeneratedBinary += savePathUtf8;
            projectGeneratedBinary += "/";

            string noUndefinedFlag;
// TODO: This causes issues with -ldl in bgfx example, where dlclose symbol is not found even though it's linked with -ldl
#if 0
            if(!onlyZigFiles)
            {
                bool isCompilerClangLike = (compilerType == RuntimeCompilerType::CLANG) || (compilerType == RuntimeCompilerType::EMSCRIPTEN);
                if(usesCppFiles)
                    noUndefinedFlag = isCompilerClangLike ? "-Wl,-undefined,error" : "-Wl,--no-undefined,--as-needed";
                else
                    noUndefinedFlag = isCompilerClangLike ? "-Wl,-undefined,error" : "-Wl,--no-undefined,--as-needed";
            }
#endif

            ninja::NinjaVariable zigObjectArgs("object_args");
            string objectZigArgs;
            if(onlyZigFiles)
                objectZigArgs = combineObjectFilesAsZigArgs(objectNames);
            ninja::NinjaArgValue zigObjectArgsValue = { zigObjectArgs, move(objectZigArgs) };
            
            switch (libraryType)
            {
                case LibraryType::EXECUTABLE:
                {
                    vector<ninja::NinjaArg> buildExeArgs;
                    vector<ninja::NinjaArg> lastBuildExeArgs;
                    string executableName = config.getPackageName();
                    if(isSamePlatformFamily(config.platform, PLATFORM_WIN))
                        executableName += ".exe";
                    if(onlyZigFiles)
                    {
                        buildExeArgs.insert(buildExeArgs.end(), {
                            ninja::NinjaArg::createRaw("zig build-exe"), 
                            //ninja::NinjaArg::createRaw("--name __tmp_zig"),
                            ninja::NinjaArg::createRaw("--name $out"),
                            ninja::NinjaArg::createRaw("$object_args"),
                        });
                        buildExeArgs.insert(buildExeArgs.end(), commonZigArgs.begin(), commonZigArgs.end());

                        vector<ninja::NinjaArg> zigLibraryFlags = convertCLibrariesToZigLibraries(config.getCompiler(), allLinkerFlags);
                        buildExeArgs.insert(buildExeArgs.end(), zigLibraryFlags.begin(), zigLibraryFlags.end());

                        ninja::NinjaRule *buildExeRule = ninjaBuildFile.createRule("build_exec", buildExeArgs);
                        buildExeRule->build(objectNames, executableName, { zigObjectArgsValue });
                    }
                    else
                    {
                        switch (config.getCompiler())
                        {
                            case Compiler::MINGW_W64:
                            case Compiler::GCC:
                            {
                                if(usesCppFiles) {
                                    buildExeArgs.push_back(ninja::NinjaArg::createRaw(cppCompilerName));
                                } else {
                                    buildExeArgs.push_back(ninja::NinjaArg::createRaw(cCompilerName));
                                }
                                string rpath = extractDynamicLibDirsFromLinkerFlags(dynamicLinkerFlags);
                                buildExeArgs.insert(buildExeArgs.end(), {
                                    ninja::NinjaArg::createRaw("-o"), 
                                    ninja::NinjaArg::createRaw("$out"), 
                                    ninja::NinjaArg::createRaw("$in"),
                                    ninja::NinjaArg::createRaw(noUndefinedFlag)
                                });

                                if(compilerType == RuntimeCompilerType::EMSCRIPTEN)
                                {
                                    buildExeArgs.push_back(ninja::NinjaArg::createRaw("-s MAIN_MODULE=1"));
                                }

                                if(!rpath.empty())
                                    buildExeArgs.push_back(ninja::NinjaArg("-Wl,-rpath," + rpath));

                                ninja::NinjaArg sanitizeFlag = getlinkerSanitizerFlags(config);
                                if(sanitizeFlag.type != ninja::NinjaArg::Type::NONE)
                                    buildExeArgs.push_back(std::move(sanitizeFlag));
                                else if(config.getOptimizationLevel() == OPT_LEV_RELEASE)
                                {
                                    // Strip binary
                                    if(!config.include_debug_symbols_in_release)
                                        buildExeArgs.push_back(ninja::NinjaArg::createRaw("-s"));
                                }

                                if(config.use_lto)
                                {
                                    buildExeArgs.push_back(ninja::NinjaArg::createRaw("-flto"));
                                }

                                // TODO: Add flag to disable -ldl and -lm (dlopen, dlclose, floor, max, ...)
#if OS_TYPE == OS_TYPE_OPENBSD || OS_TYPE == OS_TYPE_HAIKU
                                buildExeArgs.insert(buildExeArgs.end(), {
                                    ninja::NinjaArg::createRaw("-lm")
                                });
#else
                                if(!isSamePlatformFamily(config.platform, PLATFORM_WIN))
                                {
                                    lastBuildExeArgs.insert(lastBuildExeArgs.end(), {
                                        ninja::NinjaArg::createRaw("-ldl"),
                                        ninja::NinjaArg::createRaw("-lm")
                                    });
                                }
#endif
                                break;
                            }
                            case Compiler::MSVC:
                            {
                                buildExeArgs.push_back(ninja::NinjaArg::createRaw(cppCompilerName));
                                // TODO: Do not link all of these. Find a way to only link the ones that are needed
                                buildExeArgs.insert(buildExeArgs.end(), {
                                    ninja::NinjaArg::createRaw("$in"),
                                    ninja::NinjaArg::createRaw("/Fe$out"),
                                    ninja::NinjaArg::createRaw("Ws2_32.lib"),
                                    ninja::NinjaArg::createRaw("Wldap32.lib"),
                                    ninja::NinjaArg::createRaw("Crypt32.lib"),
                                    ninja::NinjaArg::createRaw("Advapi32.lib"),
                                    ninja::NinjaArg::createRaw("Gdi32.lib"),
                                    ninja::NinjaArg::createRaw("User32.lib"),
                                    ninja::NinjaArg::createRaw("Userenv.lib"),
                                    ninja::NinjaArg::createRaw("OpenGL32.lib"),
                                    ninja::NinjaArg::createRaw("GlU32.lib"),
                                    ninja::NinjaArg::createRaw("Shell32.lib")
                                });

                                switch (SYSTEM_PLATFORM)
                                {
                                    case PLATFORM_WIN32:
                                        buildExeArgs.push_back(ninja::NinjaArg::createRaw("/MACHINE:X86"));
                                        break;
                                    case PLATFORM_WIN64:
                                        buildExeArgs.push_back(ninja::NinjaArg::createRaw("/MACHINE:X64"));
                                        break;
                                }
                                break;
                            }
                        }

                        if (!allLinkerFlags.empty())
                            buildExeArgs.push_back(ninja::NinjaArg::createRaw(allLinkerFlags));

                        buildExeArgs.insert(buildExeArgs.end(), lastBuildExeArgs.begin(), lastBuildExeArgs.end());

                        buildExeArgs.push_back(ninja::NinjaArg::createRaw(packagingFlags));
                        if(config.getCompiler() == Compiler::MINGW_W64)
                        {
                            buildExeArgs.insert(buildExeArgs.end(), {
                                ninja::NinjaArg::createRaw("-lws2_32"),
                                ninja::NinjaArg::createRaw("-lwldap32"),
                                ninja::NinjaArg::createRaw("-lcrypt32"),
                                ninja::NinjaArg::createRaw("-ladvapi32"),
                                ninja::NinjaArg::createRaw("-lgdi32"),
                                ninja::NinjaArg::createRaw("-luser32"),
                                ninja::NinjaArg::createRaw("-luserenv"),
                                ninja::NinjaArg::createRaw("-lopengl32"),
                                ninja::NinjaArg::createRaw("-lglu32"),
                                ninja::NinjaArg::createRaw("-Wl,-Bstatic -lwinpthread -Wl,-Bdynamic"),
                                ninja::NinjaArg::createRaw("-lshell32")
                            });
                        }
                        ninja::NinjaRule *buildExeRule = ninjaBuildFile.createRule("build_exec", buildExeArgs);
                        ninja::NinjaBuild *ninjaBuild = buildExeRule->build(objectNames, executableName, {});
                        ninjaBuild->alwaysRun = true;
                    }
                    
                    projectGeneratedBinary += executableName;
                    projectGeneratedBinary += "\"";
                    break;
                }
                case LibraryType::STATIC:
                {
                    vector<ninja::NinjaArg> buildStaticArgs;
                    string generatedFile;
                    switch (config.getCompiler())
                    {
                        case Compiler::MINGW_W64:
                        case Compiler::GCC:
                        {
                            generatedFile = "lib" + config.getPackageName() + "." + toUtf8(CONFIG_STATIC_LIB_FILE_EXTENSION);
                            break;
                        }
                        case Compiler::MSVC:
                        {
                            generatedFile = config.getPackageName() + "." + toUtf8(CONFIG_STATIC_LIB_FILE_EXTENSION);
                            break;
                        }
                    }

                    if(onlyZigFiles)
                    {
                        buildStaticArgs.insert(buildStaticArgs.end(), {
                            ninja::NinjaArg::createRaw("zig build-lib --static"), 
                            //ninja::NinjaArg::createRaw("--name __tmp_zig"),
                            ninja::NinjaArg::createRaw("--name $out"),
                            ninja::NinjaArg::createRaw("$object_args")
                        });
                        buildStaticArgs.insert(buildStaticArgs.end(), commonZigArgs.begin(), commonZigArgs.end());

                        ninja::NinjaRule *buildStaticRule = ninjaBuildFile.createRule("build_static", buildStaticArgs);
                        buildStaticRule->build(objectNames, generatedFile, { zigObjectArgsValue });
                    }
                    else
                    {
                        switch (config.getCompiler())
                        {
                            case Compiler::MINGW_W64:
                            case Compiler::GCC:
                            {
                                buildStaticArgs.push_back(ninja::NinjaArg::createRaw(compilerLinker));
                                buildStaticArgs.insert(buildStaticArgs.end(), {
                                    ninja::NinjaArg::createRaw("rcs"),
                                    ninja::NinjaArg::createRaw("$out"),
                                    ninja::NinjaArg::createRaw("$in")
                                });
                                break;
                            }
                            case Compiler::MSVC:
                            {
                                buildStaticArgs.push_back(ninja::NinjaArg::createRaw(compilerLinker));
                                buildStaticArgs.insert(buildStaticArgs.end(), {
                                    ninja::NinjaArg::createRaw("/OUT:$out"),
                                    ninja::NinjaArg::createRaw("$in")
                                });

                                switch (SYSTEM_PLATFORM)
                                {
                                    case PLATFORM_WIN32:
                                        buildStaticArgs.push_back(ninja::NinjaArg::createRaw("/MACHINE:X86"));
                                        break;
                                    case PLATFORM_WIN64:
                                        buildStaticArgs.push_back(ninja::NinjaArg::createRaw("/MACHINE:X64"));
                                        break;
                                }
                                break;
                            }
                        }

                        ninja::NinjaRule *buildStaticRule = ninjaBuildFile.createRule("build_static", buildStaticArgs);
                        ninja::NinjaBuild *ninjaBuild = buildStaticRule->build(objectNames, generatedFile, {});
                        ninjaBuild->alwaysRun = true;
                    }
                    
                    projectGeneratedBinary += generatedFile;
                    projectGeneratedBinary += "\"";
                    if(parentProjStaticLinkerFlagCallbackFunc)
                        parentProjStaticLinkerFlagCallbackFunc(projectGeneratedBinary);
                    
                    break;
                }
                case LibraryType::DYNAMIC:
                {
                    vector<ninja::NinjaArg> buildDynamicArgs;
                    vector<ninja::NinjaArg> lastBuildDynamicArgs;
                    string generatedFile;
                    switch (config.getCompiler())
                    {
                        case Compiler::MINGW_W64:
                        case Compiler::GCC:
                        {
                            string fileExtension = toUtf8(CONFIG_DYNAMIC_LIB_FILE_EXTENSION);
                            if(compilerType == RuntimeCompilerType::EMSCRIPTEN)
                                fileExtension = "wasm";
                            else if(config.getCompiler() == Compiler::MINGW_W64)
                                fileExtension = "dll";
                            generatedFile = "lib" + config.getPackageName() + "." + fileExtension;
                            break;
                        }
                        case Compiler::MSVC:
                        {
                            generatedFile = config.getPackageName() + "." + toUtf8(CONFIG_DYNAMIC_LIB_FILE_EXTENSION);
                            break;
                        }
                    }

                    if(onlyZigFiles)
                    {
                        buildDynamicArgs.insert(buildDynamicArgs.end(), {
                            ninja::NinjaArg::createRaw("zig build-lib --rdynamic"), 
                            //ninja::NinjaArg::createRaw("--name __tmp_zig"),
                            ninja::NinjaArg::createRaw("--name $out"),
                            ninja::NinjaArg::createRaw("$object_args")
                        });
                        buildDynamicArgs.insert(buildDynamicArgs.end(), commonZigArgs.begin(), commonZigArgs.end());

                        vector<ninja::NinjaArg> zigLibraryFlags = convertCLibrariesToZigLibraries(config.getCompiler(), allLinkerFlags);
                        buildDynamicArgs.insert(buildDynamicArgs.end(), zigLibraryFlags.begin(), zigLibraryFlags.end());

                        ninja::NinjaRule *buildDynamicRule = ninjaBuildFile.createRule("build_dynamic", buildDynamicArgs);
                        buildDynamicRule->build(objectNames, generatedFile, { zigObjectArgsValue });
                    }
                    else
                    {
                        switch (config.getCompiler())
                        {
                            case Compiler::MINGW_W64:
                            case Compiler::GCC:
                            {
                                if(usesCppFiles) {
                                    buildDynamicArgs.push_back(ninja::NinjaArg::createRaw(cppCompilerName));
                                } else {
                                    buildDynamicArgs.push_back(ninja::NinjaArg::createRaw(cCompilerName));
                                }
                                buildDynamicArgs.insert(buildDynamicArgs.end(), {
                                    ninja::NinjaArg::createRaw("$in"), 
                                    ninja::NinjaArg::createRaw("-shared"),
                                    ninja::NinjaArg("-Wl,-soname," + generatedFile),
                                    ninja::NinjaArg::createRaw("-o"),
                                    ninja::NinjaArg::createRaw("$out"),
                                    ninja::NinjaArg::createRaw(noUndefinedFlag)
                                });

                                if(compilerType == RuntimeCompilerType::EMSCRIPTEN)
                                {
                                    buildDynamicArgs.push_back(ninja::NinjaArg::createRaw("-s SIDE_MODULE=1"));
                                }

                                ninja::NinjaArg sanitizeFlag = getlinkerSanitizerFlags(config);
                                if(sanitizeFlag.type != ninja::NinjaArg::Type::NONE)
                                    buildDynamicArgs.push_back(std::move(sanitizeFlag));

                                if(config.use_lto)
                                {
                                    buildDynamicArgs.push_back(ninja::NinjaArg::createRaw("-flto"));
                                }

                                // TODO: Add flag to disable -ldl and -lm (dlopen, dlclose, floor, max, ...)
#if OS_TYPE == OS_TYPE_OPENBSD || OS_TYPE == OS_TYPE_HAIKU
                                buildDynamicArgs.insert(buildDynamicArgs.end(), {
                                    ninja::NinjaArg::createRaw("-lm")
                                });
#else
                                if(!isSamePlatformFamily(config.platform, PLATFORM_WIN))
                                {
                                    lastBuildDynamicArgs.insert(lastBuildDynamicArgs.end(), {
                                        ninja::NinjaArg::createRaw("-ldl"),
                                        ninja::NinjaArg::createRaw("-lm")
                                    });
                                }
#endif
                                break;
                            }
                            case Compiler::MSVC:
                            {
                                buildDynamicArgs.push_back(ninja::NinjaArg::createRaw(compilerLinker));
                                buildDynamicArgs.insert(buildDynamicArgs.end(), {
                                    ninja::NinjaArg::createRaw("/OUT:$out"),
                                    ninja::NinjaArg::createRaw("$in"),
                                    ninja::NinjaArg::createRaw("Ws2_32.lib"),
                                    ninja::NinjaArg::createRaw("Wldap32.lib"),
                                    ninja::NinjaArg::createRaw("Crypt32.lib"),
                                    ninja::NinjaArg::createRaw("Advapi32.lib"),
                                    ninja::NinjaArg::createRaw("Gdi32.lib"),
                                    ninja::NinjaArg::createRaw("User32.lib"),
                                    ninja::NinjaArg::createRaw("Userenv.lib"),
                                    ninja::NinjaArg::createRaw("OpenGL32.lib"),
                                    ninja::NinjaArg::createRaw("GlU32.lib"),
                                    ninja::NinjaArg::createRaw("Shell32.lib")
                                });

                                switch (SYSTEM_PLATFORM)
                                {
                                    case PLATFORM_WIN32:
                                        buildDynamicArgs.push_back(ninja::NinjaArg::createRaw("/MACHINE:X86"));
                                        break;
                                    case PLATFORM_WIN64:
                                        buildDynamicArgs.push_back(ninja::NinjaArg::createRaw("/MACHINE:X64"));
                                        break;
                                }
                                break;
                            }
                        }

                        if (!allLinkerFlags.empty())
                            buildDynamicArgs.push_back(ninja::NinjaArg::createRaw(allLinkerFlags));

                        buildDynamicArgs.insert(buildDynamicArgs.end(), lastBuildDynamicArgs.begin(), lastBuildDynamicArgs.end());

                        buildDynamicArgs.push_back(ninja::NinjaArg::createRaw(packagingFlags));
                        if(config.getCompiler() == Compiler::MINGW_W64)
                        {
                            buildDynamicArgs.insert(buildDynamicArgs.end(), {
                                ninja::NinjaArg::createRaw("-lws2_32"),
                                ninja::NinjaArg::createRaw("-lwldap32"),
                                ninja::NinjaArg::createRaw("-lcrypt32"),
                                ninja::NinjaArg::createRaw("-ladvapi32"),
                                ninja::NinjaArg::createRaw("-lgdi32"),
                                ninja::NinjaArg::createRaw("-luser32"),
                                ninja::NinjaArg::createRaw("-luserenv"),
                                ninja::NinjaArg::createRaw("-lopengl32"),
                                ninja::NinjaArg::createRaw("-lglu32"),
                                ninja::NinjaArg::createRaw("-Wl,-Bstatic -lwinpthread -Wl,-Bdynamic"),
                                ninja::NinjaArg::createRaw("-lshell32")
                            });
                        }
                        ninja::NinjaRule *buildDynamicRule = ninjaBuildFile.createRule("build_dynamic", buildDynamicArgs);
                        ninja::NinjaBuild *ninjaBuild = buildDynamicRule->build(objectNames, generatedFile, {});
                        ninjaBuild->alwaysRun = true;
                    }
                    
                    projectGeneratedBinary += generatedFile;
                    projectGeneratedBinary += "\"";
                    if(parentProjDynamicLinkerFlagCallbackFunc)
                        parentProjDynamicLinkerFlagCallbackFunc(projectGeneratedBinary);
                    
                    break;
                }
                default:
                    assert(false);
                    return Result<bool>::Err("Unexpected error");
            }
        }

        if(!sourceFiles.empty())
        {
            string result = ninjaBuildFile.generate();
            Result<bool> fileOverwriteResult = sibs::fileOverwrite(ninjaBuildFilePath.data.c_str(), sibs::StringView(result.data(), result.size()));
            if (fileOverwriteResult.isErr())
                return fileOverwriteResult;

            Result<bool> buildResult = compile(savePath);
            if (!buildResult)
                return buildResult;
            
            if((config.isMainProject() && !config.shouldBuildTests()) || config.isTest())
            {
                buildResult = buildCompilationDatabase(savePath, config.getProjectPath());
                if(!buildResult)
                    return buildResult;
            }
        }

        Result<bool> buildTestResult = buildTests(allLinkerFlags, projectGeneratedBinary, config, cflags, dependencyExportIncludeDirs, dynamicLinkerFlags);
        if(!buildTestResult)
            return buildTestResult;

        return Result<bool>::Ok(true);
    }

    Result<bool> Ninja::buildTests(const std::string &parentLinkerFlags, const std::string &parentGeneratedLib, const SibsConfig &config, const vector<ninja::NinjaArg> &parentCflags, const string &parentDependencyExportIncludeDirs, const std::vector<std::string> &parentDynamicLinkerFlags)
    {
        if(testSourceDir.empty() || !config.shouldBuildTests())
            return Result<bool>::Ok(true);

        assert(getNinjaLibraryType(config.getPackageType()) != LibraryType::EXECUTABLE);
        
        string parentProjectPathUtf8 = toUtf8(config.getProjectPath());
        string parentExportIncludeDirs = parentDependencyExportIncludeDirs;
        for (const string &parentGlobalIncludeDir : config.getGlobalIncludeDirs())
        {
            string parentExportIncludeDir = parentProjectPathUtf8;
            parentExportIncludeDir += "/";
            parentExportIncludeDir += parentGlobalIncludeDir;
            
            parentExportIncludeDirs += " ";
            parentExportIncludeDirs += getIncludeOptionFlag(config.getCompiler(), parentExportIncludeDir);
        }

        Path testSourceDirNative = toFileString(testSourceDir);
        Path projectConfFilePath = Path(testSourceDirNative).join("project.conf");

        FileType projectConfFileType = getFileType(projectConfFilePath.data.c_str());
        SibsTestConfig sibsTestConfig(config.getCompiler(), testSourceDirNative.data, config.getOptimizationLevel());
        sibsTestConfig.platform = config.platform;
        sibsTestConfig.setSanitize(config.getSanitize());
        sibsTestConfig.zigTestFiles = move(config.zigTestFiles);
        sibsTestConfig.zigTestAllFiles = config.zigTestAllFiles;
        if(projectConfFileType == FileType::REGULAR)
        {
            Result<bool> result = Config::readFromFile(projectConfFilePath.data.c_str(), sibsTestConfig);
            if(!result)
                return result;
        }

        backend::Ninja ninja;
        ninja.addGlobalIncludeDirs(parentExportIncludeDirs);
        ninja.customCflags = std::move(parentCflags);
        if(!parentLinkerFlags.empty())
            ninja.addDependency(parentLinkerFlags);

        for(const std::string &parentDynamicLinkerFlag : parentDynamicLinkerFlags)
        {
            ninja.addDynamicDependency(parentDynamicLinkerFlag);
        }

        switch(config.getPackageType())
        {
            case PackageType::STATIC:
                ninja.addDependency(parentGeneratedLib);
                break;
            case PackageType::LIBRARY:
            case PackageType::DYNAMIC:
                ninja.addDynamicDependency(parentGeneratedLib);
                break;
            default:
                break;
        }

        bool zigTest = false;
        if(config.zigTestAllFiles)
        {
            backend::BackendUtils::collectSourceFiles(testSourceDirNative.data.c_str(), &ninja, sibsTestConfig);
            // TODO: This can be optimized as well. No need to insert non-zig files if we are going to remove them.
            // Maybe pass a filter callback function to @collectSourceFiles.
            for(auto it = ninja.sourceFiles.begin(); it != ninja.sourceFiles.end(); )
            {
                if(it->language != sibs::Language::ZIG)
                {
                    it = ninja.sourceFiles.erase(it);
                }
                else
                {
                    ++it;
                }
            }
            zigTest = true;
        }
        else if(!config.zigTestFiles.empty())
        {
            ninja.sourceFiles.reserve(config.zigTestFiles.size());
            for(const sibs::FileString &testFile : config.zigTestFiles)
            {
                ninja.addSourceFile(sibs::Language::ZIG, toUtf8(testFile.c_str()).c_str());
            }
            zigTest = true;
        }
        else
        {
            backend::BackendUtils::collectSourceFiles(testSourceDirNative.data.c_str(), &ninja, sibsTestConfig);
        }

        if(!ninja.getSourceFiles().empty())
        {
            Path buildPath = Path(testSourceDirNative).join(TINYDIR_STRING("sibs-build")).join(toFileString(asString(config.platform)));
            switch(sibsTestConfig.getOptimizationLevel())
            {
                case OPT_LEV_DEBUG:
                    buildPath.join(TINYDIR_STRING("debug"));
                    break;
                case OPT_LEV_RELEASE:
                    buildPath.join(TINYDIR_STRING("release"));
                    break;
            }
            
            Result<bool> buildFileResult = ninja.build(sibsTestConfig, buildPath.data.c_str());
            if (!buildFileResult)
                return buildFileResult;

            if(!zigTest)
            {
                Result<ExecResult> runTestResult = exec({ Path(buildPath).join(toFileString(sibsTestConfig.getPackageName())).data }, true);
                if(!runTestResult)
                    return Result<bool>::Err(runTestResult);
                
                if(runTestResult.unwrap().exitCode != 0)
                    return Result<bool>::Err("Tests failed", runTestResult.unwrap().exitCode);
            }
        }

        return Result<bool>::Ok(true);
    }

    Result<bool> Ninja::compile(const _tinydir_char_t *buildFilePath)
    {
        Result<ExecResult> execResult = exec({ TINYDIR_STRING("ninja"), TINYDIR_STRING("-C"), buildFilePath }, true);
        if(execResult.isOk())
        {
            if(execResult.unwrap().exitCode == 0)
            {
                //printf("%s\n", execResult.unwrap().execStdout.c_str());
                return Result<bool>::Ok(true);
            }
            else
                return Result<bool>::Err("");
        }
        else
            return Result<bool>::Err("");
    }
    
    Result<bool> Ninja::buildCompilationDatabase(const _tinydir_char_t *buildFilePath, const FileString &saveDir)
    {
        Result<ExecResult> execResult = exec({
            TINYDIR_STRING("ninja"), TINYDIR_STRING("-C"), buildFilePath, TINYDIR_STRING("-t"),
            TINYDIR_STRING("compdb"), TINYDIR_STRING("compile_c"), TINYDIR_STRING("compile_cpp")
        }, false);
        if(execResult)
        {
            if(execResult.unwrap().exitCode == 0)
            {
                fileOverwrite(Path(saveDir).join("compile_commands.json").data.c_str(), { execResult.unwrap().execStdout.data(), execResult.unwrap().execStdout.size() });
            }
            else
            {
                string errMsg = "Failed to build compilation database, reason: ";
                errMsg += execResult.unwrap().execStdout;
                return Result<bool>::Err(errMsg);
            }
        }
        else
        {
            string errMsg = "Failed to build compilation database, reason: ";
            errMsg += execResult.getErrMsg();
            return Result<bool>::Err(errMsg);
        }
        return Result<bool>::Ok(true);
    }
}
