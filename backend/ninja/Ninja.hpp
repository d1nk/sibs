#ifndef BACKEND_NINJA_HPP
#define BACKEND_NINJA_HPP

#include "../../include/Dependency.hpp"
#include "../../include/Result.hpp"
#include "../../include/Linker.hpp"
#include "../../include/Conf.hpp"
#include <vector>
#include <string>
#include <functional>
#include <ninja/Ninja.hpp>

namespace backend
{
    class Ninja;
    
    struct NinjaSubProject
    {
        Ninja *subProject;
        sibs::SibsConfig *config;
        sibs::FileString buildPath;
        
        NinjaSubProject() : subProject(nullptr), config(nullptr) {}
        NinjaSubProject(Ninja *_subProject, sibs::SibsConfig *_config, sibs::FileString &&_buildPath) : 
            subProject(_subProject), 
            config(_config), 
            buildPath(move(_buildPath))
        {
            
        }
    };
    
    class Ninja
    {
    public:
        enum class LibraryType
        {
            EXECUTABLE,
            STATIC,
            DYNAMIC,
        };

        Ninja();

        void addGlobalIncludeDirs(const std::string &globalIncludeDirs);
        void addSourceFile(sibs::Language language, const char *filepath);
        void setTestSourceDir(const char *dir);
        void addDependency(const std::string &binaryFile);
        void addDynamicDependency(const std::string &dependency);
        void addSubProject(Ninja *subProject, sibs::SibsConfig *config, sibs::FileString &&buildPath);
        const std::vector<sibs::SourceFile>& getSourceFiles() const;
        sibs::Result<bool> build(const sibs::SibsConfig &config, const _tinydir_char_t *savePath, sibs::LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc = nullptr, sibs::LinkerFlagCallbackFunc dynamicLinkerFlagCallback = nullptr, sibs::GlobalIncludeDirCallbackFunc globalIncludeDirCallback = nullptr);

        std::vector<ninja::NinjaArg> customCflags;
    private:
        sibs::Result<bool> buildSubProjects(sibs::LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc, sibs::LinkerFlagCallbackFunc dynamicLinkerFlagCallback, sibs::GlobalIncludeDirCallbackFunc globalIncludeDirCallback);
        sibs::Result<bool> buildTests(const std::string &parentLinkerFlags, const std::string &parentGeneratedLib, const sibs::SibsConfig &config, const std::vector<ninja::NinjaArg> &parentCflags, const std::string &parentDependencyExportIncludeDirs, const std::vector<std::string> &parentDynamicLinkerFlags);
        bool containsSourceFile(const std::string &filepath) const;
        bool containsDependency(const std::string &dependency) const;
        bool containsDynamicDependency(const std::string &dependency) const;
        sibs::Result<bool> getLinkerFlags(const sibs::SibsConfig &config, sibs::LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc, sibs::LinkerFlagCallbackFunc dynamicLinkerFlagCallback, sibs::GlobalIncludeDirCallbackFunc globalIncludeDirCallback, sibs::CflagsCallbackFunc cflagsCallbackFunc) const;
        sibs::Result<bool> compile(const _tinydir_char_t *buildFilePath);
        sibs::Result<bool> buildCompilationDatabase(const _tinydir_char_t *buildFilePath, const sibs::FileString &savePath);
    private:
        std::string customGlobalIncludeDirs;
        std::string testSourceDir;
        std::vector<sibs::SourceFile> sourceFiles;
        std::vector<std::string> binaryDependencies;
        std::vector<std::string> dynamicDependencies;
        std::vector<NinjaSubProject> subProjects;
    };
}

#endif //BACKEND_NINJA_HPP
