#include <catch2/catch.hpp>
#include "../../../include/Conf.hpp"

using namespace sibs;

static FileString arg_list_to_string(const std::vector<FileString> &args) {
    FileString result;
    for(const FileString &arg : args) {
        if(!result.empty())
            result += TINYDIR_STRING(" ");
        result += TINYDIR_STRING("\"") + arg + TINYDIR_STRING("\"");
    }
    return result;
}

TEST_CASE("parse config")
{
	SibsConfig sibsConfig(Compiler::GCC, TINYDIR_STRING("tests/src/confTest"), OPT_LEV_DEBUG, false);
    Result<bool> result = Config::readFromFile(TINYDIR_STRING("tests/src/confTest/validProject.conf"), sibsConfig);
    if(result.isErr())
    {
        fprintf(stderr, "%s", result.getErrMsg().c_str());
        exit(1);
    }
    REQUIRE(sibsConfig.getPackageName() == "confTest");
    REQUIRE(sibsConfig.getPackageType() == PackageType::LIBRARY);
    
    REQUIRE(sibsConfig.getPlatforms().size() == 2);
    REQUIRE(containsPlatform(sibsConfig.getPlatforms(), PLATFORM_LINUX64));
    REQUIRE(containsPlatform(sibsConfig.getPlatforms(), PLATFORM_WIN64));
    
    REQUIRE(sibsConfig.getPackageListDependencies().size() == 2);
    for(const auto &dep : sibsConfig.getPackageListDependencies())
    {
        REQUIRE(dep.getSource() == Dependency::Source::PACKAGE_LIST);
    }
    auto *xxhashDependency = sibsConfig.getPackageListDependencies()[0].asPackageListDependency();
    REQUIRE(xxhashDependency->name == "xxhash");
    REQUIRE(xxhashDependency->version.toString() == ">=0.1.0 and <1.0.0");
    
    const auto &catch2Dependency = sibsConfig.getPackageListDependencies()[1].asPackageListDependency();
    REQUIRE(catch2Dependency->name == "catch2");
    REQUIRE(catch2Dependency->version.toString() == ">=1.0.0 and <2.0.0");
    
    REQUIRE(sibsConfig.shouldUseCmake());
    
    REQUIRE(sibsConfig.getCmakeDir() == TINYDIR_STRING("tests/src/confTest/cmakeGlobal"));
    REQUIRE(arg_list_to_string(sibsConfig.getCmakeArgs()) == "\"-G\" \"Ninja\" \"-DCMAKE_BUILD_TYPE=Debug\" \"-DENTITYX_RUN_BENCHMARKS=0\"");
    
    REQUIRE(sibsConfig.getCmakeDirStatic() == TINYDIR_STRING("tests/src/confTest/cmakeStatic"));
    REQUIRE(arg_list_to_string(sibsConfig.getCmakeArgsStatic()) == "\"-G\" \"Ninja\" \"-DCMAKE_BUILD_TYPE=Debug\" \"-DENTITYX_RUN_BENCHMARKS=0\" \"-DENTITYX_BUILD_TESTING=0\"");
    
    REQUIRE(sibsConfig.getCmakeDirDynamic() == TINYDIR_STRING("tests/src/confTest/cmakeDynamic"));
    REQUIRE(arg_list_to_string(sibsConfig.getCmakeArgsDynamic()) == "\"-G\" \"Ninja\" \"-DCMAKE_BUILD_TYPE=Debug\" \"-DENTITYX_RUN_BENCHMARKS=0\" \"-DENTITYX_BUILD_TESTING=0\" \"-DENTITYX_BUILD_SHARED=1\"");
}

TEST_CASE("parse config - invalid object")
{
	SibsConfig sibsConfig(Compiler::GCC, TINYDIR_STRING("tests/src/confTest"), OPT_LEV_DEBUG, false);
    Result<bool> result = Config::readFromFile(TINYDIR_STRING("tests/src/confTest/invalidObject.conf"), sibsConfig);
    REQUIRE(result.isErr());
    REQUIRE(result.getErrMsg().find("Invalid config object \"invalidObj\"") != std::string::npos);
}

TEST_CASE("parse config - invalid field")
{
	SibsConfig sibsConfig(Compiler::GCC, TINYDIR_STRING("tests/src/confTest"), OPT_LEV_DEBUG, false);
    Result<bool> result = Config::readFromFile(TINYDIR_STRING("tests/src/confTest/invalidField.conf"), sibsConfig);
    REQUIRE(result.isErr());
    REQUIRE(result.getErrMsg().find("Invalid field \"invalidField\" under object \"package\"") != std::string::npos);
}

TEST_CASE("parse config - use different config for different platforms")
{
    SibsConfig sibsConfig(Compiler::GCC, TINYDIR_STRING("tests/src/confTest"), OPT_LEV_DEBUG, false);
    Result<bool> result = Config::readFromFile(TINYDIR_STRING("tests/src/confTest/platformConfig.conf"), sibsConfig);
    if(result.isErr())
    {
        fprintf(stderr, "%s", result.getErrMsg().c_str());
        exit(1);
    }
    
    #if OS_TYPE == OS_TYPE_LINUX and defined(SIBS_ENV_64BIT)
        REQUIRE(sibsConfig.getDebugStaticLibs()[0] == TINYDIR_STRING("tests/src/confTest/linux/x64/static/debug/libcool.a"));
        REQUIRE(sibsConfig.getReleaseStaticLibs()[0] == TINYDIR_STRING("tests/src/confTest/linux/x64/static/release/libcool.a"));
        REQUIRE(sibsConfig.getGlobalIncludeDirs()[0] == "include_linux64");
    #elif OS_TYPE == OS_TYPE_LINUX and defined(SIBS_ENV_32BIT)
        REQUIRE(sibsConfig.getDebugStaticLibs()[0] == TINYDIR_STRING("tests/src/confTest/linux/x86/static/debug/libcool.a"));
        REQUIRE(sibsConfig.getReleaseStaticLibs()[0] == TINYDIR_STRING("tests/src/confTest/linux/x86/static/release/libcool.a"));
        REQUIRE(sibsConfig.getGlobalIncludeDirs()[0] == "include_linux32");
    #endif
}

TEST_CASE("parse config - define static")
{
    SibsConfig sibsConfig(Compiler::GCC, TINYDIR_STRING("tests/src/confTest"), OPT_LEV_DEBUG, false);
    Result<bool> result = Config::readFromFile(TINYDIR_STRING("tests/src/confTest/defineStatic.conf"), sibsConfig);
    if(result.isErr())
    {
        fprintf(stderr, "%s", result.getErrMsg().c_str());
        exit(1);
    }
    
    REQUIRE(sibsConfig.getDefinedValue("GLOBAL_DEFINE") == "1");
    REQUIRE(sibsConfig.getDefinedValue("BUILD_STATIC") == "1");
    REQUIRE(sibsConfig.getDefinedValue("DEFINE_TYPE") == "STATIC");
}

TEST_CASE("parse config - define dynamic")
{
    SibsConfig sibsConfig(Compiler::GCC, TINYDIR_STRING("tests/src/confTest"), OPT_LEV_DEBUG, false);
    Result<bool> result = Config::readFromFile(TINYDIR_STRING("tests/src/confTest/defineDynamic.conf"), sibsConfig);
    if(result.isErr())
    {
        fprintf(stderr, "%s", result.getErrMsg().c_str());
        exit(1);
    }
    
    REQUIRE(sibsConfig.getDefinedValue("GLOBAL_DEFINE") == "1");
    REQUIRE(sibsConfig.getDefinedValue("BUILD_STATIC") == "0");
    REQUIRE(sibsConfig.getDefinedValue("DEFINE_TYPE") == "DYNAMIC");
}
