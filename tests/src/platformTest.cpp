#include <catch2/catch.hpp>
#include "../../include/Platform.hpp"

using namespace sibs;

TEST_CASE("platform same family")
{
    REQUIRE(isSamePlatformFamily(PLATFORM_LINUX, PLATFORM_LINUX_X86_64));
    REQUIRE(isSamePlatformFamily(PLATFORM_LINUX_X86_64, PLATFORM_LINUX));
    REQUIRE(!isSamePlatformFamily(PLATFORM_LINUX_X86_64, PLATFORM_MACOS));
    REQUIRE(!isSamePlatformFamily(PLATFORM_MACOS, PLATFORM_LINUX_X86_64));
    REQUIRE(!isSamePlatformFamily(PLATFORM_MACOS, PLATFORM_LINUX));
    REQUIRE(!isSamePlatformFamily(PLATFORM_LINUX, PLATFORM_MACOS));
}
