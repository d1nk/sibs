#include <catch2/catch.hpp>
#include "../../include/VersionParser.hpp"

using namespace sibs;

TEST_CASE("parse package version")
{
    VersionParser parser;
    {
        Result<PackageVersionRange> parseResult = parser.parse("17.3.4", 6);
        if(!parseResult)
            FAIL(parseResult.getErrMsg());
        PackageVersionRange versionRange = parseResult.unwrap();
        REQUIRE(versionRange.startDefined);
        REQUIRE(versionRange.endDefined);
        REQUIRE(versionRange.startOperation == VersionOperation::GREATER_EQUAL);
        REQUIRE(versionRange.start.major == 17);
        REQUIRE(versionRange.start.minor == 3);
        REQUIRE(versionRange.start.patch == 4);
        REQUIRE(versionRange.endOperation == VersionOperation::LESS);
        REQUIRE(versionRange.end.major == 18);
        REQUIRE(versionRange.end.minor == 0);
        REQUIRE(versionRange.end.patch == 0);
    }
    {
        Result<PackageVersionRange> parseResult = parser.parse("2.3", 3);
        if(!parseResult)
            FAIL(parseResult.getErrMsg());
        PackageVersionRange versionRange = parseResult.unwrap();
        REQUIRE(versionRange.startDefined);
        REQUIRE(versionRange.endDefined);
        REQUIRE(versionRange.startOperation == VersionOperation::GREATER_EQUAL);
        REQUIRE(versionRange.start.major == 2);
        REQUIRE(versionRange.start.minor == 3);
        REQUIRE(versionRange.start.patch == 0);
        REQUIRE(versionRange.endOperation == VersionOperation::LESS);
        REQUIRE(versionRange.end.major == 3);
        REQUIRE(versionRange.end.minor == 0);
        REQUIRE(versionRange.end.patch == 0);
    }
    {
        Result<PackageVersionRange> parseResult = parser.parse("2", 1);
        if(!parseResult)
            FAIL(parseResult.getErrMsg());
        PackageVersionRange versionRange = parseResult.unwrap();
        REQUIRE(versionRange.startDefined);
        REQUIRE(versionRange.endDefined);
        REQUIRE(versionRange.startOperation == VersionOperation::GREATER_EQUAL);
        REQUIRE(versionRange.start.major == 2);
        REQUIRE(versionRange.start.minor == 0);
        REQUIRE(versionRange.start.patch == 0);
        REQUIRE(versionRange.endOperation == VersionOperation::LESS);
        REQUIRE(versionRange.end.major == 3);
        REQUIRE(versionRange.end.minor == 0);
        REQUIRE(versionRange.end.patch == 0);
    }
    {
        Result<PackageVersionRange> parseResult = parser.parse(">=2.3.4 and <5.6.7", 18);
        if(!parseResult)
            FAIL(parseResult.getErrMsg());
        PackageVersionRange versionRange = parseResult.unwrap();
        REQUIRE(versionRange.startDefined);
        REQUIRE(versionRange.endDefined);
        REQUIRE(versionRange.startOperation == VersionOperation::GREATER_EQUAL);
        REQUIRE(versionRange.start.major == 2);
        REQUIRE(versionRange.start.minor == 3);
        REQUIRE(versionRange.start.patch == 4);
        REQUIRE(versionRange.endOperation == VersionOperation::LESS);
        REQUIRE(versionRange.end.major == 5);
        REQUIRE(versionRange.end.minor == 6);
        REQUIRE(versionRange.end.patch == 7);
    }
    {
        Result<PackageVersionRange> parseResult = parser.parse(">2.3.4 and <=5.6.7", 18);
        if(!parseResult)
            FAIL(parseResult.getErrMsg());
        PackageVersionRange versionRange = parseResult.unwrap();
        REQUIRE(versionRange.startDefined);
        REQUIRE(versionRange.endDefined);
        REQUIRE(versionRange.startOperation == VersionOperation::GREATER);
        REQUIRE(versionRange.start.major == 2);
        REQUIRE(versionRange.start.minor == 3);
        REQUIRE(versionRange.start.patch == 4);
        REQUIRE(versionRange.endOperation == VersionOperation::LESS_EQUAL);
        REQUIRE(versionRange.end.major == 5);
        REQUIRE(versionRange.end.minor == 6);
        REQUIRE(versionRange.end.patch == 7);
    }
    {
        Result<PackageVersionRange> parseResult = parser.parse(">2.3.4 and <=5", 14);
        if(!parseResult)
            FAIL(parseResult.getErrMsg());
        PackageVersionRange versionRange = parseResult.unwrap();
        REQUIRE(versionRange.startDefined);
        REQUIRE(versionRange.endDefined);
        REQUIRE(versionRange.startOperation == VersionOperation::GREATER);
        REQUIRE(versionRange.start.major == 2);
        REQUIRE(versionRange.start.minor == 3);
        REQUIRE(versionRange.start.patch == 4);
        REQUIRE(versionRange.endOperation == VersionOperation::LESS_EQUAL);
        REQUIRE(versionRange.end.major == 5);
        REQUIRE(versionRange.end.minor == 0);
        REQUIRE(versionRange.end.patch == 0);
    }
    {
        REQUIRE(!parser.parse("", 0));
        REQUIRE(!parser.parse(" ", 1));
        REQUIRE(!parser.parse("2", 2));
        REQUIRE(!parser.parse(".", 1));
        REQUIRE(!parser.parse(".2", 2));
        REQUIRE(!parser.parse("<", 1));
        REQUIRE(!parser.parse("and", 3));
        REQUIRE(!parser.parse("2 and", 5));
        REQUIRE(!parser.parse("2 and 3", 7));
        REQUIRE(!parser.parse("2 and =3", 8));
        REQUIRE(!parser.parse("2 and >3", 8));
        REQUIRE(!parser.parse(">=1.0.0 and 2.0.0", 17));
        REQUIRE(!parser.parse("1.0.0 < 2.0.0", 13));
        REQUIRE(!parser.parse("1.0.0 and <0.9.3", 16));
        REQUIRE(!parser.parse("=1.0.0 and <2.0.0", 17));
        REQUIRE(!parser.parse("1.0.0 and <2.0.0", 16));
        REQUIRE(!parser.parse(">=1.0.0 and =2.0.0", 18));
    }
}
