#!/usr/bin/env python3

import sys
import os
import mingw_ldd
import shutil

def usage():
    print("usage: mingw-package.py <executable_path>")
    exit(1)

def main():
    if len(sys.argv) != 2:
        usage()
    
    executable_path = sys.argv[1]
    if not os.path.isfile(executable_path):
        print("arg executable_path is not a file or it's a directory")
        exit(2)
    executable_path = os.path.realpath(executable_path)
    executable_dir = os.path.dirname(executable_path)

    print("Checking dynamic library dependencies of %s..." % executable_path)
    deps = mingw_ldd.dep_tree(executable_path)
    for dll, full_path in deps.items():
        if full_path != "not found":
            print("Copying %s to %s" % (dll, executable_dir))
            shutil.copyfile(full_path, os.path.join(executable_dir, dll))

if __name__ == "__main__":
    main()