#!/bin/sh

if (( "$#" != 1 )); then
    echo "usage: download_dependencies.sh <program_name>"
    exit 1
fi

is_root=0
if [ $(id -u) = 0 ]; then
    is_root=1
fi

program_name="$1"
script_path=$(readlink -f "$0")
script_dir=$(dirname "$script_path")

if [ -f /usr/lib/sibs/"$program_name".cache ]; then
    #echo "No need to download dependencies, all dependencies exist in cache"
    exit 0
fi

if [ $is_root -eq 0 ] && [ -f ~/.local/lib/sibs/"$program_name".cache ]; then
    #echo "No need to download dependencies, all dependencies exist in cache"
    exit 0
fi

command -v sha1sum > /dev/null || { echo "Missing program: sha1sum"; exit 1; }
command -v wget > /dev/null || { echo "Missing program: wget"; exit 1; }

set -e
IFS=$'\n' GLOBIGNORE='*' command eval 'dependencies=($(cat "$script_dir"/dependencies.conf))'
IFS=$'\n' GLOBIGNORE='*' command eval 'urls=($(cat "$script_dir"/urls.conf))'
IFS=$'\n' GLOBIGNORE='*' command eval 'libmaps=($(cat "$script_dir"/libmap.conf))'

if (( "${#dependencies[@]}" % 2 != 0 )); then
    echo "Invalid number of arguments in dependencies.conf file. Expected multiple libraries which are the dependencies, followed by their checksum"
    exit 4
fi

program_libs_dir=""
if [ $is_root -eq 0 ]; then
    mkdir -p ~/.local/lib/sibs
    program_libs_dir=~/.local/lib/sibs/"$program_name"
else
    mkdir -p /usr/lib/sibs
    program_libs_dir=/usr/lib/sibs/"$program_name"
fi
mkdir -p "$program_libs_dir"
set +e

echoerr() { echo "$@" 1>&2; }

for (( i=0; i<${#dependencies[@]}; i=$i+2 ))
do
    file=${dependencies[i]}
    checksum_file="$file".sha1
    checksum=${dependencies[i+1]}

    if [ -f /usr/lib/sibs/"$file" ] && [ "$checksum" == "$(cat /usr/lib/sibs/$checksum_file)" ]; then
        echoerr "Using sibs global lib file $file"
    elif [ $is_root -eq 0 ] && [ -f ~/.local/lib/sibs/"$file" ] && [ "$checksum" == "$(cat ~/.local/lib/sibs/$checksum_file)" ]; then
        echoerr "Using sibs user lib file $file"
    elif [ -f /usr/lib/"$file" ] && echo "$checksum" /usr/lib/"$file" | sha1sum -c --status; then
        echoerr "Using system lib file $file"
        set -e
        if [ $is_root -eq 0 ]; then
            cp /usr/lib/"$file" ~/.local/lib/sibs/"$file"
            echo "$checksum" > ~/.local/lib/sibs/"$checksum_file"
        else
            cp /usr/lib/"$file" /usr/lib/sibs/"$file"
            echo "$checksum" > /usr/lib/sibs/"$checksum_file"
        fi
        set +e
    else
        downloaded=0
        for url in "${urls[@]}"; do
            dst_dir=""
            if [ $is_root -eq 0 ]; then
                dst_dir=~/.local/lib/sibs
            else
                dst_dir=/usr/lib/sibs
            fi

            echoerr "Downloading missing library from ${url}${file}"
            if wget -q -O "$dst_dir/$file" "$url""$file"; then
                echo "$checksum" > "$dst_dir/$checksum_file"
                downloaded=1
                break
            fi
        done

        if [ $downloaded -eq 0 ]; then
            echo "Failed to download dependency $file from all urls"
            exit 2
        fi
    fi
done

for (( i=0; i<${#libmaps[@]}; i=$i+2 ))
do
    src=${libmaps[i]}
    dst=${libmaps[i+1]}

    lib_file=""
    if [ -f /usr/lib/sibs/"$src" ]; then
        lib_file=/usr/lib/sibs/"$src"
    elif [ $is_root -eq 0 ] && [ -f ~/.local/lib/sibs/"$src" ]; then
        lib_file=~/.local/lib/sibs/"$src"
    elif [ -f /usr/lib/"$src" ]; then
        lib_file=/usr/lib/"$src"
    fi

    #echo "Creating symlink for lib file $dst"
    ln -sf "$lib_file" "$program_libs_dir/$dst"
    if [ $? -ne 0 ]; then
        echo "Failed to create symlink for program"
        exit 3
    fi
done

set -e
if [ $is_root -eq 0 ]; then
    touch ~/.local/lib/sibs/"$program_name".cache
else
    touch /usr/lib/sibs/"$program_name".cache
fi
