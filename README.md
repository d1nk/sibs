# Simple Build System for Native Languages
Sibs is inspired by [Cargo](https://github.com/rust-lang/cargo/), you can think of it like a C/C++/Zig version of Cargo. Sibs can build cmake projects as well, so you can use sibs with existing cmake projects with minimal work.
List of packages can be found at https://git.dec05eba.com/sibs-packages/plain/packages.json (currently in progress of moving existing sibs packages from github to git.dec05eba.com).\
Packages available on your system accessible by pkg-config are also used.\
Projects in subdirectories will also be built as dependencies (these are not set in project.conf). The recommended way is to use git submodules.

# Usage
```
Usage: sibs COMMAND

Simple Build System for Native Languages

Commands:
  build         Build a project that contains a project.conf file
  run           Build and run a project that contains a project.conf file
  new           Create a new project
  init          Initialize project in an existing directory
  test          Build and run tests for a sibs project
  package       Create a redistributable package from a sibs project. Note: Redistributable packages can't use system packages to build
  platform      Print name of platform (to stdout) and exit
  platforms     Print list of supported platforms (to stdout) and exit
```

Example flow:
```
# Create the project
sibs new project-name --exec
cd project-name
# Build the project in debug mode and run it
sibs run
# To compile with optimizations enabled, run:
# sibs build --release
# and then the output binary is available in sibs-build/<platform-name>/release/project-name
```

# Examples
Here is a minimal config file:
```
[package]
name = "hello_world"
type = "executable"
version = "0.1.0"
platforms = ["any"]
```
There are full project examples with dependencies in the examples directory.
Use `sibs init` to create a project, which includes a config file to get started and then build with with `sibs build`
and run the binary under `sibs-build/<platform>/debug/<executable_name>`.

### Supported platforms
|Linux|Windows(1)|MacOS|OpenBSD |Haiku|(Webassembly) Emscripten   |...   |
|-----|----------|-----|--------|-----|---------------------------|------|
|✓    |✓         |✓    |✓      |✓    |✓                          |TBD(2)|

(1). Msvc, mingw-w64 and cygwin are supported. Cygwin is defined as a linux platform while mingw-w64 is defined as a windows system.\
(2). Sibs is intended to work on as many platforms as possible, you can help by porting sibs to another platform. Should only be minor changes if the platform is unix-like.

Linux is the primary platform, the platform which master branch is guaranteed to compile on.

# Dependencies
`libcurl, libarchive, ninja, cmake, pkg-config, ccache`

# Installation
## Posix (Linux, MacOS, OpenBSD, Haiku)
Run `./cmake/install.sh` as root user.
## Arch Linux
On Arch Linux, sibs can alternatively be found on aur under the name `sibs-git` (`yay -S sibs-git`).
## Windows
Use vcpkg to install the required dependencies and then generate visual studio (or another system) build files using CMakeLists.txt
# Usage
After you have installed sibs, execute `sibs` without any arguments and you will get a list of commands and description for them. For debug builds, the created binary/library files will be located under `sibs-build/<platform>/debug`. For example on linux x86_64, the path for binaries would be: `sibs-build/linux_x86_64/debug`.\
The current recommended way of using sibs is to add dependencies as git submodules and they will automatically be used. To use system dependencies use pkg-config.\
You can list all your system packages with `pkg-config --list-all` (if the packages provide pkg-config files). To search for a package in this list you can use grep, like so: `pkg-config --list-all | grep 'pkg-name'` (replace pkg-name with the name of the package you want to search for).\
To then show the version of the found package you can use `pkg-config --modversion pkg-name`.
# Quirks
Zig support has not been tested properly yet and currently always links to c library.
You can run zig tests with `sibs test --file filepath` or `sibs test --all-files`.
Currently zig tests are cached because ninja build system is used, which means if source files do not change between runs.
Currently zig files generate header files and include exported functions into `sibs-build/<platform>/generated-headers/zig` and the generated headers
are usable from c/c++ by using including: `#include <zig/INSERT_ZIG_HEADER_FILE_NAME_HERE>`.
If your project contains zig files then it will currently only run on Linux, Windows and MacOS as zig doesn't support more platforms at the moment.
# Package
Sibs supports creating a redistributable packages of projects (currently only on Linux, run `sibs package --bundle`). Packaging is in testing phase and may not work for all projects. Currently you need to have python3 and ldd installed and also set the environment variable SIBS_SCRIPT_DIR to scripts sub directory which is in sibs root directory (the directory that contains package.py).
Currently a script file is generated which should be used to run the project. The name of the script file is the same as project. This script file will most likely to be removed later. Do NOT run the executable called "program".
Because creating a package is currently done by copying c/c++ libraries and precompiled shared libraries on Linux usually depend on gcc runtime libraries which are very large, the distributable package becomes very large; a hello world application extracted from its archive is 6 megabytes...
If you want to reduce the size of your package then you will have to compile your project and each dependency from source with clang/musl (gcc c++ runtime is 14mb while clang c++ runtime is 800kb!).

The package command also comes with --bundle-install option which reduces the size of the distributable package by removing libraries in the package that can be downloaded online, and instead the user will download missing libraries when launching the application for the first time (the libraries are cached). This option is good because if the user already has the libraries installed on their system with a package managed then the user dont have to download the libraries and if the user has other software that was distributed using sibs, then their libraries will be shared with your projects; meaning if one project has a library of one version then it's shared with all software that uses same version of the library.

Users are required to manually install some libraries as they can't be included in a distributed package (install with their package manager). These libraries are commonly gpu driver libraries, which vary even if you have the same cpu architecture.
This requirement might be removed later, if the gpu driver libraries required can somehow be detected and downloaded cross platform.
Libraries that are downloaded are available at: https://github.com/DEC05EBA/libraries
# Cross compilation
Automatic cross compilation (`sibs build --platform <platform>`)currently only works from linux_x86_64 to win64 by using mingw-w64. You need to install `mingw-w64-gcc` and optionally `mingw-w64-pkg-config` if you want to use mingw-w64 system installed packages.
Cross compilation does currently not work if you have zig files as zig doesn't support libc when cross compiling at the moment.
You can run `scripts/mingw_package.py` to automatically copy dynamic library dependencies of your executable to the same directory as the executable, so the library can be found when running the executable on windows; this also allows you to bundle your application and distribute it without external dependencies. To run `scripts/mingw_package.py` you need to install pefile python library `sudo pip install pefile`.

Manual cross compilation can be done by replacing c, c++ compilers and static library archiver (ar) using the environment variable CC, CXX and AR.
# IDE support
Sibs generates a compile_commands.json in the project root directory when executing `sibs build` and tools that support clang completion can be used, such as YouCompleteMe or cquery.
There are several editors that support YouCompleteMe, including Vim, Emacs and Visual Studio Code. Visual studio code now also supports clang completion with C/C++ extension by Microsoft. I recommend using Visual Studio Code along with cquery (https://github.com/cquery-project/cquery/wiki), which gives you very good IDE support for your C/C++ projects.
If you are using Visual Studio Code then you should add .vscode/ to .gitignore or Visual Studio Code will lag a lot (because cquery adds a lot of files in .vscode directory).
# Tests
If your project contains a sub directory called "tests" then that directory will be used a test project. The test directory may contain a project.conf file which can contain \[dependencies] block for specifying test only dependencies. The test automatically includes the parent project as a dependency.
# Project configuration template
```toml
[package]
name = "packageName"
type = "library"
version = "0.1.0"
platforms = ["any"]
authors = ["DEC05EBA <0xdec05eba@gmail.com>"]

[dependencies]
catch2 = "0.1.0"
xxhash = "0.1.0"

[lang.c]
version = "c11"

[lang.cpp]
version = "c++14"
enable_exceptions = "true"

[define]
BOOST_ASIO_SEPERATE_COMPILATION = "1"

[define.static]
BOOST_COMPILE_STATIC = "1"

[define.dynamic]
BOOST_COMPILE_DYNAMIC = "1"

[config]
include_dirs = ["include", "/opt/cuda/targets/x86_64-linux/include"]
libs = ["/usr/lib/libcuda.so"]
ignore_dirs = ["examples"]
expose_include_dirs = ["include"]
error_on_warning = "true"

[config.win32.static.debug]
lib = "windows/x86/static/debug"

[config.win32.static.release]
lib = "windows/x86/static/release"

[config.win64.static.debug]
lib = "windows/x64/static/debug"

[cmake]
dir = "."
args = ["ENTITYX_RUN_BENCHMARKS=0"]

[cmake.static]
args = ["ENTITYX_BUILD_SHARED=0"]

[cmake.dynamic]
args = ["ENTITYX_BUILD_SHARED=1"]
```
## package
### name
Required
### type
Required. Should be one of: "executable", "static", "dynamic", "library"
### version
Required. Version string has to be in the format of "xxx.yyy.zzz" where xxx is major, yyy is minor and zzz is patch. Version format is based on [semver 2.0.0](https://semver.org/spec/v2.0.0.html)
### platforms
Required. A list of platforms the package supports. Run `sibs platforms` to view a list of supported platforms.
If platforms contains "any" then there is no need to specify other platforms
### authors
Optional. A list of authors
## dependencies
Optional. A list of dependencies which are specified in name-value pairs where the name is the name of the dependency, which should match the dependency name under the packages name specified in its project.conf file.
The value should be a version string, which specified the range of versions that you want to accept as a dependency to only allow dependency version that has the features you need and the version which hasn't changed its interface.
These are examples of the version string format:
```
# Version 1.0.0 or above and less than 2.0.0, same as >=1.0.0 and <2.0.0
1.0.0
# Version 1.0.0 or above
>=1.0.0
# Version above 1.0.0
>1.0.0
# Version exactly 1.0.0
=1.0.0
# Version less than 1.0.0
<1.0.0
# Version 1.0 or above but less than 2.0
1.0 and <2.0
# Version above 1.0 but less or equal to 1.3.2
>1 and <=1.3.2
```
Dependencies are automatically choosen from system (linux, mac) or if no package manager exists, then it's download from an url (see https://git.dec05eba.com/sibs-packages/plain/packages.json).
The dependency can also be a git project, in which case it will have the fields 'git' and optionally 'branch' and 'revision'.
'git' specifies the url to the git repository, 'branch' is the git branch that should be used - defaults to 'master'.
'revision' is the git revision to checkout, defaults to 'HEAD' (latest commit).

Dependencies can also be added to a project but adding sub directories with project.conf file.
The best way to do this is to create another git project for the dependency and then adding that git project as a git submodule.
Using sub projects allows you to modify dependency and propagate changes to dependant project without pushing changes to remote git repository (faster development).
## lang.*
Optional. Allows you to change language specific configuration. \[lang.c] is for C and \[lang.cpp] is for C++.
## lang.c
### version
Optional. The c standard version to use. Should be either ansi (alias for c89), c89, c99, c11 or c17. The default value is c11.
## lang.cpp
### version
Optional. The c++ standard version to use. Should be either c++03, c++98, c++11, c++14, c++17 or c++20. The default value is c++14.
### enable_exceptions
Optional. This option should be either "true" or "false" and specifies if exceptions should be enabled for the project. The default value is "true".
## define
Optional. A list of definitions which are specified in name-value pairs where the name is the preprocessor to define (in c: #define name value)
## define.static
Works like \[define], but these definitions are only used when building static project. If a definition with the same exists in \[define], then it's overwritten
## define.dynamic
Works like \[define], but these definitions are only used when building dynamic project. If a definition with the same exists in \[define], then it's overwritten
## config
### include_dirs
Optional. A list of directories which should be specified as global include directories when compiling. This means that instead of using relative paths to header files, you can include the directory with headers and then you only have to specify the header name when using #include.\
You can use the `$out` variable to specify the build directory.
### libs
Optional. A list of additional external libraries which should be linked with the program.
### ignore_dirs
Optional. A list of directories to ignore. This means that if the ignored directory contains source files, then they wont be included in the build.
### expose_include_dirs
Optional. A list of directories which contains (header) files which should be exposed to dependencies as directories to include globally. This means that dependencies can include (header) files from the dependency without specifying path to the dependency.\
You can use the `$out` variable to specify the build directory.
### error_on_warning
Optional. This option should be either "true" or "false" and specifies if compiler warnings for the project (and not its dependencies) should work warnings as errors.
Default value is "false".
## config.*
Optional. The name is structured in the following way: config.platform.libraryType.optimizationLevel
where platform is any of the platforms specified under \[package] (or if package contains "any", then it can be any other platform). LibraryType is either "static" or "dynamic" - different configurations depending on if the package is included as a static or dynamic library by a dependant package. OptimizationLevel is either "debug" or "release", depending on which optimization level the "root" package was built with ("root" package is usually the project which is an executable)
### lib
Optional. A directory which contains .lib or .dll files which should be included in dependant projects that uses this project
## cmake
Optional. Using this allows you to build cmake projects. If a project contains cmake in the project.conf file, then sibs wont build the project itself
and will use cmake instead. Sibs will put the built executable and library files into the same location they would be if sibs build them,
meaning you can have dependency to a cmake project from a sibs project and it will automatically use the dependency library files
### dir
Optional. Directory that contains CMakeLists.txt. If this is not specified, the project root will be used (same location where project.conf is located)
### args
Optional. List of arguments to cmake. The arguments should be in the same format as "-D" arguments (options) in cmake, except they should exclude "-D".
Do not use CMAKE_BUILD_TYPE as sibs will automatically use it depending on the optimization level the user specifies when building project.
## cmake.*
Optional. The name is structured in the following way: config.libraryType 
where libraryType is either "static" or "dynamic" - different configurations depending on if the package is included as a static or dynamic library by a dependant package.
Args specified under \[cmake.static] or \[cmake.dynamic] are appended to the args specified under \[cmake]

# TODO
* Make shell scripts portable. Currently they only work with bash... Use shellcheck to find the issues.
* Use `-Wl,-Bstatic` and `-Wl,-Bdynamic` when linking libraries, otherwise it's not possible to build with musl that prefers static libraries and will give an error if the static version doesn't exist.
