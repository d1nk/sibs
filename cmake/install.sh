#!/bin/sh

set -e

[ $(id -u) -ne 0 ] && echo "You need root privileges to run the install script" && exit 1

case "$(uname -s)" in
	Linux*)		machine="Linux" ;;
	Darwin*)	machine="Mac" ;;
	OpenBSD*)	machine="OpenBSD" ;;
	*)		echo "The install file can only be run on linux, mac and openbsd" && exit 1 ;;
esac

scriptpath="$(dirname "$0")"
mkdir -p "$scriptpath/build/release"
cd "$scriptpath/build/release"
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release ../../../
ninja

case $machine in
	Linux) 		bin_dir="/usr/bin" ;;
	Mac)		bin_dir="/usr/local/bin" ;;
	OpenBSD)	bin_dir="/usr/local/bin" ;;
esac

install -Dm755 sibs "$bin_dir/sibs"
echo "Copied $scriptpath/build/release/sibs to $bin_dir/sibs"
echo "Installation successful!"
