#!/usr/bin/env bash

set -e

scriptpath="$(dirname "$0")"
mkdir -p "$scriptpath/build/debug"
cd "$scriptpath/build/debug"
cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug ../../../
ninja
echo "Debug build success"
