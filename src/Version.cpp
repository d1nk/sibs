#include "../include/Version.hpp"
#include <cassert>

namespace sibs
{
    const char* asString(VersionOperation operation)
    {
        switch(operation)
        {
            case VersionOperation::LESS:                return "<";
            case VersionOperation::LESS_EQUAL:          return "<=";
            case VersionOperation::EQUAL:               return "=";
            case VersionOperation::GREATER:             return ">";
            case VersionOperation::GREATER_EQUAL:       return ">=";
            default:                                    return "";
        }
    }

    std::string PackageVersion::toString() const
    {
        std::string result;
        result += std::to_string(major);
        result += ".";
        result += std::to_string(minor);
        result += ".";
        result += std::to_string(patch);
        return result;
    }

    std::string PackageVersionRange::toString() const
    {
        std::string result;
        result += asString(startOperation);
        result += start.toString();
        if(endDefined)
        {
            result += " and ";
            result += asString(endOperation);
            result += end.toString();
        }
        return result;
    }

    static bool isInRangeOfEnd(const PackageVersionRange &versionRange, const PackageVersion &version)
    {
        if(!versionRange.endDefined)
            return true;

        switch(versionRange.endOperation)
        {
            case VersionOperation::LESS:
                return version < versionRange.end;
            case VersionOperation::LESS_EQUAL:
                return version <= versionRange.end;
            default:
                assert(false);
                return true;
        }
    }

    bool PackageVersionRange::isInRange(const PackageVersion &version) const
    {
        switch(startOperation)
        {
            case VersionOperation::LESS:
                return version < start;
            case VersionOperation::LESS_EQUAL:
                return version <= start;
            case VersionOperation::EQUAL:
                return version == start;
            case VersionOperation::GREATER:
            {
                if(version <= start)
                    return false;
                return isInRangeOfEnd(*this, version);
            }
            case VersionOperation::GREATER_EQUAL:
            {
                if(version < start)
                    return false;
                return isInRangeOfEnd(*this, version);
            }
        }
        return true;
    }
}