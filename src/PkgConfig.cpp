#include "../include/PkgConfig.hpp"
#include "../include/Exec.hpp"
#include "../include/Dependency.hpp"
#include "../include/VersionParser.hpp"

using namespace std;

static sibs::FileString pkgConfigPath = TINYDIR_STRING("pkg-config");

// TODO: Do not use pkg-config program. The same functionality can easily be achieved
// by reading files in /usr/share/pkgconfig
// Or is there no downside to calling pkg-config program?
namespace sibs
{
    static string trimRight(const string &input)
    {
        for(int i = input.size() - 1; i >= 0; --i)
        {
            if(!isspace(input[i]))
                return input.substr(0, i + 1);
        }
        return "";
    }

    void PkgConfig::setPkgConfigPath(const FileString &path)
    {
        pkgConfigPath = path;
    }
    
    Result<bool> PkgConfig::validatePkgConfigPackageVersionExists(const PackageListDependency &dependency)
    {
        Result<bool> dependencyValidationResult = PkgConfig::validatePackageExists(dependency.name);
        if(dependencyValidationResult.isErr())
            return Result<bool>::Err(dependencyValidationResult.getErrMsg());

        Result<PackageVersion> dependencyVersionResult = PkgConfig::getPackageVersion(dependency.name);
        if(!dependencyVersionResult)
            return Result<bool>::Err(dependencyVersionResult);

        if(!dependency.version.isInRange(dependencyVersionResult.unwrap()))
            return Result<bool>::Err("pkg-config package " + dependency.name + " exists but the version does not match our expected version range");

        return Result<bool>::Ok(true);
    }

    Result<bool> PkgConfig::validatePackageExists(const string &name)
    {
        Result<ExecResult> execResult = exec({ pkgConfigPath, TINYDIR_STRING("--exists"), TINYDIR_STRING("--"), toFileString(name) });
        if(execResult.isErr())
            return Result<bool>::Err(execResult.getErrMsg());

        if(execResult.unwrap().exitCode == 1)
        {
            string errMsg = "pkg-config dependency not found: ";
            errMsg += name;
            return Result<bool>::Err(errMsg);
        }
        else if(execResult.unwrap().exitCode == 127)
        {
            return Result<bool>::Err("pkg-config is not installed");
        }
        else if(execResult.unwrap().exitCode != 0)
        {
            string errMsg = "Failed to check if dependency exists, Unknown error, exit code: ";
            errMsg += to_string(execResult.unwrap().exitCode);
            return Result<bool>::Err(errMsg);
        }

        return Result<bool>::Ok(true);
    }

    Result<PackageVersion> PkgConfig::getPackageVersion(const std::string &name)
    {
        Result<ExecResult> execResult = exec({ pkgConfigPath, TINYDIR_STRING("--modversion"), TINYDIR_STRING("--"), toFileString(name) });
        if(!execResult)
            return Result<PackageVersion>::Err(execResult.getErrMsg());

        if(execResult.unwrap().exitCode == 1)
        {
            string errMsg = "Dependency ";
            errMsg += name;
            errMsg += " not found in pkg-config";
            return Result<PackageVersion>::Err(errMsg);
        }
        else if(execResult.unwrap().exitCode == 127)
        {
            return Result<PackageVersion>::Err("pkg-config is not installed");
        }
        else if(execResult.unwrap().exitCode != 0)
        {
            string errMsg = "Failed to get pkg-config package version, Unknown error, exit code: ";
            errMsg += to_string(execResult.unwrap().exitCode);
            return Result<PackageVersion>::Err(errMsg);
        }

        // Intentionally allow packages which have a version that contains more data after patch number, since some pkg-config packages are not in semver format
        return parsePackageVersion({ execResult.unwrap().execStdout.data(), execResult.unwrap().execStdout.size() }, nullptr);
    }

    Result<string> PkgConfig::getDynamicLibsLinkerFlags(const vector<PackageListDependency> &libs)
    {
        if(libs.empty()) return Result<string>::Ok("");

        std::vector<FileString> args = { pkgConfigPath, TINYDIR_STRING("--libs"), TINYDIR_STRING("--") };
        for(const PackageListDependency &lib : libs)
        {
            args.push_back(toFileString(lib.name));
        }

        Result<ExecResult> execResult = exec(args);
        if(execResult.isErr())
            return Result<string>::Err(execResult.getErrMsg());

        if(execResult.unwrap().exitCode == 0)
        {
            execResult.unwrap().execStdout = trimRight(execResult.unwrap().execStdout);
            return Result<string>::Ok(execResult.unwrap().execStdout);
        }
        else if(execResult.unwrap().exitCode == 1)
        {
            // TODO: This shouldn't happen because we check if each dependency is installed before this,
            // but maybe the package is uninstalled somewhere between here...
            // Would be better to recheck if each package is installed here again
            // to know which package was uninstalled
            return Result<string>::Err("Packages not found");
        }
        else if(execResult.unwrap().exitCode == 127)
        {
            return Result<string>::Err("pkg-config is not installed");
        }
        else
        {
            string errMsg = "Failed to get package dynamic lib linking flags, Unknown error, exit code: ";
            errMsg += to_string(execResult.unwrap().exitCode);
            return Result<string>::Err(errMsg);
        }
    }
    
    Result<string> PkgConfig::getDynamicLibsCflags(const vector<PackageListDependency> &libs)
    {
        if(libs.empty()) return Result<string>::Ok("");

        std::vector<FileString> args = { pkgConfigPath, TINYDIR_STRING("--cflags"), TINYDIR_STRING("--") };
        for(const PackageListDependency &lib : libs)
        {
            args.push_back(toFileString(lib.name));
        }

        Result<ExecResult> execResult = exec(args);
        if(execResult.isErr())
            return Result<string>::Err(execResult.getErrMsg());

        if(execResult.unwrap().exitCode == 0)
        {
            execResult.unwrap().execStdout = trimRight(execResult.unwrap().execStdout);
            return Result<string>::Ok(execResult.unwrap().execStdout);
        }
        else if(execResult.unwrap().exitCode == 1)
        {
            // TODO: This shouldn't happen because we check if each dependency is installed before this,
            // but maybe the package is uninstalled somewhere between here...
            // Would be better to recheck if each package is installed here again
            // to know which package was uninstalled
            return Result<string>::Err("Packages not found");
        }
        else if(execResult.unwrap().exitCode == 127)
        {
            return Result<string>::Err("pkg-config is not installed");
        }
        else
        {
            string errMsg = "Failed to get package dynamic lib cflags, Unknown error, exit code: ";
            errMsg += to_string(execResult.unwrap().exitCode);
            return Result<string>::Err(errMsg);
        }
    }
    
    Result<PkgConfigFlags> PkgConfig::getDynamicLibsFlags(const vector<PackageListDependency> &libs)
    {
        PkgConfigFlags flags;
        
        Result<string> linkerFlagsResult = getDynamicLibsLinkerFlags(libs);
        if(!linkerFlagsResult)
            return Result<PkgConfigFlags>::Err(linkerFlagsResult);
        
        Result<string> cflagsResult = getDynamicLibsCflags(libs);
        if(!cflagsResult)
            return Result<PkgConfigFlags>::Err(cflagsResult);
        
        flags.linkerFlags = move(linkerFlagsResult.unwrap());
        flags.cflags = move(cflagsResult.unwrap());
        return Result<PkgConfigFlags>::Ok(flags);
    }
}