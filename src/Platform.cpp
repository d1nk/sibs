#include "../include/Platform.hpp"
#include <cassert>
#include <algorithm>

namespace sibs
{
    static std::unordered_map<Platform, const char*> platformNames;
    static bool platformNamesSet = false;

    // Returns -1 if no bit is set
    static u64 getLeastSignificantBitSetPosition(u64 value)
    {
        for(u64 i = 0; i < sizeof(u64); ++i)
        {
            if(value & 1ULL)
                return i;
            value >>= 1ULL;
        }
        return -1ULL;
    }

    bool containsPlatform(const std::vector<Platform> &platforms, Platform platform)
    {
        for (Platform vecPlatform : platforms)
        {
            if(platform & vecPlatform)
                return true;
        }
        return false;
    }

    const char* asString(Platform platform)
    {
        if(!platformNamesSet)
        {
            platformNamesSet = true;
            for(auto &it : PLATFORM_BY_NAME)
            {
                platformNames[it.second] = it.first.data;
            }
        }

        auto it = platformNames.find(platform);
        if(it != platformNames.end())
            return it->second;
        return nullptr;
    }

    Platform getPlatformByName(StringView name)
    {
        auto it = PLATFORM_BY_NAME.find(name);
        if(it != PLATFORM_BY_NAME.end())
            return it->second;
        return PLATFORM_INVALID;
    }

    static std::string join(const std::vector<std::string> &strings)
    {
        std::string result;
        ssize size = strings.size();
        ssize i = 0;
        for(const std::string &str: strings)
        {
            result += str;
            if(i == size - 2)
                result += " and ";
            else if(i < size - 2)
                result += ", ";
            ++i;
        }
        return result;
    }

    std::string getPlatformListFormatted()
    {
        std::vector<std::string> platformsSorted;
        for(auto &it: PLATFORM_BY_NAME)
        {
            platformsSorted.push_back({ it.first.data, it.first.size });
        }
        std::sort(platformsSorted.begin(), platformsSorted.end());
        return join(platformsSorted);
    }

    bool isSamePlatformFamily(Platform a, Platform b)
    {
        return (a & b) == a || (b & a) == b;
    }

    bool isBaseForPlatform(Platform base, Platform platform)
    {
        return base == PLATFORM_ANY || base == platform || isSamePlatformFamily(base, platform);
    }

    Platform getPlatformGenericType(Platform platform)
    {
        if(platform == PLATFORM_INVALID || platform == PLATFORM_ANY)
            return PLATFORM_INVALID;
        return (Platform)(1ULL << (u64)getLeastSignificantBitSetPosition(platform));
    }
}
