#ifndef SIBS_STRING_VIEW_HPP
#define SIBS_STRING_VIEW_HPP

#define XXH_FORCE_NATIVE_FORMAT
#include "../external/xxhash.h"
#include "types.hpp"
#include "env.hpp"
#include <cstring>
#include <unordered_map>
#include <map>
#include <cassert>

namespace sibs
{
    class StringView
    {
    public:
        StringView() : data(nullptr), size(0)
        {
            
        }

        StringView(const StringView &other) : data(other.data), size(other.size)
        {

        }
        
        StringView(const char *_data) : data(_data), size(strlen(_data))
        {
            
        }
        
        StringView(const char *_data, usize _size) : data(_data), size(_size)
        {
            
        }
        
        bool equals(const StringView &other) const
        {
            return size == other.size && memcmp(data, other.data, size) == 0;
        }
        
        size_t hash() const
        {
        #if defined(CISB_ENV_64BIT)
            return XXH64(data, size, 0xdec05eba);
        #else
            return XXH32(data, size, 0xdec05eba);
        #endif
        }

        char operator [] (size_t index) const
        {
            assert(index < size);
            return data[index];
        }

        bool operator < (const StringView &other) const
        {
            return ((ssize)size - (ssize)other.size < 0) || strncmp(data, other.data, size) < 0;
        }

        bool operator > (const StringView &other) const
        {
            return !operator<(other);
        }

        size_t operator()() const
        {
            return hash();
        }

        bool operator()(const StringView &other) const
        {
            return equals(other);
        }

        bool operator == (const StringView &other) const
        {
            return equals(other);
        }
        
        const char *data;
        usize size;
    };
    
    struct StringViewHash
    {
        size_t operator()(const StringView &stringView) const
        {
            return stringView.hash();
        }
    };
    
    struct StringViewCompare
    {
    public:
        bool operator()(const StringView &lhs, const StringView &rhs) const
        {
            return lhs.equals(rhs);
        }
    };

    struct StringViewCompareSorted
    {
    public:
        bool operator()(const StringView &lhs, const StringView &rhs) const
        {
            return lhs < rhs;
        }
    };
    
    template <typename ValueType>
    using StringViewMap = std::unordered_map<const StringView, ValueType, StringViewHash, StringViewCompare>;

  //  template <typename ValueType>
   // using StringViewMapSorted = std::map<const StringView, ValueType, StringViewCompareSorted>;
}

#endif // SIBS_STRING_VIEW_HPP
