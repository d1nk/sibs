#ifndef SIBS_ENV_HPP
#define SIBS_ENV_HPP

#define OS_FAMILY_WINDOWS 0
#define OS_FAMILY_POSIX 1

#define OS_TYPE_WINDOWS 0
#define OS_TYPE_LINUX 1
#define OS_TYPE_APPLE 2
#define OS_TYPE_OPENBSD 3
#define OS_TYPE_HAIKU 10

#if defined(_WIN32) || defined(_WIN64)
    #define OS_FAMILY OS_FAMILY_WINDOWS
    #define OS_TYPE OS_TYPE_WINDOWS

	#ifndef UNICODE
	#define UNICODE
	#endif

	#ifndef _UNICODE
	#define _UNICODE
	#endif

	#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN
	#endif

	#include <windows.h>
#endif

#ifdef __linux__
    #define OS_FAMILY OS_FAMILY_POSIX
    #define OS_TYPE OS_TYPE_LINUX
#endif

#if defined(__APPLE__) || defined(__MACOSX__)
    #define OS_FAMILY OS_FAMILY_POSIX
    #define OS_TYPE OS_TYPE_APPLE
#endif

#ifdef __OpenBSD__
    #define OS_FAMILY OS_FAMILY_POSIX
    #define OS_TYPE OS_TYPE_OPENBSD
#endif

#ifdef __HAIKU__
    #define OS_FAMILY OS_FAMILY_POSIX
    #define OS_TYPE OS_TYPE_HAIKU
#endif

#ifdef __CYGWIN__
    #define OS_FAMILY OS_FAMILY_POSIX
    #define OS_TYPE OS_TYPE_LINUX
#endif

#ifdef __EMSCRIPTEN__
    #define OS_FAMILY OS_FAMILY_POSIX
    #define OS_TYPE OS_TYPE_LINUX
#endif

#if defined(__aarch64__)
    #define SIBS_ARCH_ARM
    #define SIBS_ENV_64BIT
#elif defined(__arm__) || defined(_ARM) || defined(_M_ARM) || defined(__arm)
    #define SIBS_ARCH_ARM
    #define SIBS_ENV_32BIT
#elif defined(__x86_64__) || defined(__amd64__) || defined(__amd64) || defined(__x86_64) || defined(_M_X64) || defined(_M_AMD64)
    #define SIBS_ARCH_X86
    #define SIBS_ENV_64BIT
#elif defined(i386) || defined(__i386) || defined(__i386__) || defined(__IA32__) || defined(_M_I86) || defined(_M_IX86) || defined(__X86__) || defined(_X86_)
    #define SIBS_ARCH_X86
    #define SIBS_ENV_32BIT
#endif

#if !defined(SIBS_ENV_32BIT) && !defined(SIBS_ENV_64BIT)
    #error "System is not detected as either 32-bit or 64-bit"
#endif

#if !defined(OS_FAMILY)
    #error "System not supported. Only Windows and Posix systems supported right now"
#endif

#if !defined(OS_TYPE)
    #error "System not supported. Only Windows, linux, macos and openbsd systems supported right now"
#endif

#if !defined(SIBS_ARCH_ARM) && !defined(SIBS_ARCH_X86)
    #error "System is not detected as either arm or x86"
#endif

#if !defined(DEBUG) && !defined(NDEBUG)
#define DEBUG
#endif

#endif // SIBS_ENV_HPP
