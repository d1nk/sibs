#ifndef SIBS_DEPENDENCY_HPP
#define SIBS_DEPENDENCY_HPP

#include <string>
#include <cassert>
#include "Version.hpp"

namespace sibs
{
    class PackageListDependency;
    
    class Dependency
    {
    public:
        enum class Source
        {
            PACKAGE_LIST,
        };
        
        virtual ~Dependency(){}
        
        virtual Source getSource() const = 0;
        
        const PackageListDependency* asPackageListDependency() const
        {
            assert(getSource() == Source::PACKAGE_LIST);
            return (PackageListDependency*)this;
        }
    };
    
    class PackageListDependency : public Dependency
    {
    public:
        virtual ~PackageListDependency(){}
        virtual Source getSource() const
        {
            return Source::PACKAGE_LIST;
        }
        
        std::string name;
        PackageVersionRange version;
    };
}

#endif //SIBS_DEPENDENCY_HPP
