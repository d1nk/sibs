#ifndef SIBS_CURL_HPP
#define SIBS_CURL_HPP

#include "Result.hpp"
#include "FileUtil.hpp"
#include <string>

namespace sibs
{
    class HttpResult
    {
    public:
        long httpCode;
        bool success;
        std::string str;
    };

    class curl
    {
    public:
        static sibs::Result<bool> downloadFile(const char *url, const _tinydir_char_t *filepath);
        static HttpResult get(const char *url);
    };
}

#endif //SIBS_CURL_HPP
