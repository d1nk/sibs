#pragma once

#include "types.hpp"
#include "Version.hpp"
#include "Result.hpp"
#include "StringView.hpp"
#include <string>

namespace sibs
{
    Result<PackageVersion> parsePackageVersion(const StringView &versionStr, int *size);

    enum class VersionToken
    {
        NONE,
        END_OF_FILE,
        INVALID,
        OPERATION,
        AND,
        VERSION_NUMBER
    };

    struct VersionTokenizer
    {
        VersionTokenizer();
        VersionTokenizer(const char *start, const usize size);
        VersionTokenizer(const VersionTokenizer &other);
        VersionToken next();


        const char *start;
        const char *code;
        usize size;
        usize index;
        PackageVersion version;
        VersionOperation operation;
        StringView identifier;
        std::string errMsg;
    };

    struct VersionParser
    {
        Result<PackageVersionRange> parse(const char *code, const usize size);

        VersionTokenizer tokenizer;
        PackageVersionRange versionRange;
    private:
        VersionToken parseStart();
        VersionToken parseEnd();
    };
}