#ifndef SIBS_FILEUTIL_HPP
#define SIBS_FILEUTIL_HPP

#ifndef UNICODE
#define UNICODE
#endif

#ifndef _UNICODE
#define _UNICODE
#endif

#include "env.hpp"
#include "../external/tinydir.h"
#include "../external/utf8/checked.h"
#include "Result.hpp"
#include "StringView.hpp"
#include <functional>

namespace sibs
{
	using FileString = std::basic_string<_tinydir_char_t, std::char_traits<_tinydir_char_t>, std::allocator<_tinydir_char_t>>;

#if OS_FAMILY == OS_FAMILY_POSIX
    std::string toUtf8(const std::string &input);
    std::string toUtf8(const char *input);
    FileString toFileString(const std::string &utf8Str);
    FileString toFileString(const StringView &utf8Str);
    FileString toFileString(const char *str);
#else
    std::string toUtf8(const sibs::FileString &input);
    std::string toUtf8(const TCHAR *input);
    FileString utf8To16(const StringView &utf8Str);
    FileString utf8To16(const std::string &utf8Str);
    FileString toFileString(const std::string &utf8Str);
    FileString toFileString(const StringView &utf8Str);
    FileString toFileString(const char *str);
    FileString getLastErrorAsString();
    void replaceChar(FileString &input, wchar_t charToReplace, wchar_t charToReplaceWith);
#endif

    class Path {
    public:
        Path(const FileString &str) : data(str) {}

        Path& join(const _tinydir_char_t *str) {
            data += TINYDIR_STRING("/");
            data += str;
            return *this;
        }

        Path& join(const Path &other) {
            data += TINYDIR_STRING("/");
            data += other.data;
            return *this;
        }

        Path& append(const FileString &str) {
            data += str;
            return *this;
        }

        FileString data;
    };

    // Return true if you want to continue iterating the remaining files, return false if you want to stop
    using FileWalkCallbackFunc = std::function<bool(tinydir_file*)>;

    struct FileWithTimestamp {
        FileString filepath;
        FileString extension;
        struct timespec last_modified;
    };

    using SortedFileWalkCallbackFunc = std::function<bool(const FileWithTimestamp&)>;

    enum class FileType
    {
        FILE_NOT_FOUND,
        REGULAR,
        DIRECTORY
    };

    FileType getFileType(const _tinydir_char_t *path);
    void walkDir(const _tinydir_char_t *directory, FileWalkCallbackFunc callbackFunc);
    void walkDirFiles(const _tinydir_char_t *directory, FileWalkCallbackFunc callbackFunc);
    bool walkDirFilesRecursive(const _tinydir_char_t *directory, FileWalkCallbackFunc callbackFunc);
    bool walkDirFilesRecursiveSortTimestamp(const _tinydir_char_t *directory, SortedFileWalkCallbackFunc callbackFunc);
    Result<std::string> getFileContent(const _tinydir_char_t *filepath);
    Result<bool> fileWrite(const _tinydir_char_t *filepath, StringView data);
    Result<bool> fileOverwrite(const _tinydir_char_t *filepath, StringView data);
    Result<FileString> getHomeDir();
    Result<FileString> getCwd();
    Result<bool> createDirectory(const _tinydir_char_t *path);
    // Note: Will not delete created directories if this operation fails for some reason
    Result<bool> createDirectoryRecursive(const _tinydir_char_t *path);
    Result<FileString> getRealPath(const _tinydir_char_t *path);
    bool pathEquals(const std::string &path, const std::string &otherPath);
    Result<u64> getFileLastModifiedTime(const _tinydir_char_t *path);
    bool isPathAbsolute(const std::string &path);
}

#endif //SIBS_FILEUTIL_HPP
