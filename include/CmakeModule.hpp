#pragma once

#include "Conf.hpp"
#include "Linker.hpp"

namespace sibs
{
    class CmakeModule
    {
    public:
        Result<bool> compile(const SibsConfig &config, const FileString &buildPath, LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc, LinkerFlagCallbackFunc dynamicLinkerFlagCallbackFunc, GlobalIncludeDirCallbackFunc globalIncludeDirCallback);
        static void setCmakePath(const FileString &path);
    };
}
