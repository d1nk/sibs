#ifndef SIBS_PACKAGEVERSION_HPP
#define SIBS_PACKAGEVERSION_HPP

#include <string>

namespace sibs
{
    /*
     * Different package version syntax:
     * ANY: "major.minor.patch" (xxx.xxx.xxx)
     *      Checks for package in several different places. First pkg-config, then local lib dir (~/.cache/sibs/lib), then git/server.
     *      Example: "2.1.002"
     * GIT: "branch:revision"
     *      Checks for package in any of the configures git servers (default: github).
     *      Example: "master:HEAD"
     * LATEST: "latest"
     *      Checks for package in several different places. First git/server then pkg-config then local lib dir (~/.cache/sibs/lib).
     *
     *
     * GIT and LATEST first check if local lib (~/.cache/sibs/lib) contains latest version by comparing the revision to remote revision.
     */
    class PackageVersion
    {
    public:
        enum class Type
        {
            ANY,
            GIT,
            LATEST
        };

        PackageVersion(int _major, int _minor, int _patch) : type(Type::ANY), major(_major), minor(_minor), patch(_patch) {}
        PackageVersion(const std::string &&_gitBranch, const std::string &&_gitRevision) :
            type(Type::GIT),
            gitBranch(_gitBranch),
            gitRevision(_gitRevision)
        {

        }

        static PackageVersion latest() { return PackageVersion(Type::LATEST); }
    private:
        PackageVersion(Type _type) : type(_type) {}
    private:
        Type type;

        int major;
        int minor;
        int patch;

        std::string gitBranch;
        std::string gitRevision;
    };
}

#endif //SIBS_PACKAGEVERSION_HPP
