#ifndef SIBS_PACKAGE_HPP
#define SIBS_PACKAGE_HPP

#include "../external/rapidjson/document.h"
#include "Platform.hpp"
#include "Result.hpp"
#include "Version.hpp"
#include <string>
#include <vector>

namespace sibs
{
    enum class PackageType : int
    {
        // Compile as executable when compiling project with this directly.
        // If used in dependency, then fail because you can't (currently) have dependency to executable.
        EXECUTABLE,

        // Compile as static library when compiling project with this directly.
        // If used in dependency, then this is the preferred library type, but the dependant project can override this.
        STATIC,

        // Compile as dynamic library when compiling project with this directly.
        // If used in dependency, then this is the preferred library type, but the dependant project can override this.
        DYNAMIC,

        // Identical to DYNAMIC
        LIBRARY
    };
    
    struct PackageMetadata
    {
        std::string description;
        PackageVersion version;
        std::vector<std::string> platforms;
        std::vector<std::string> urls;
    };
    
    class Package
    {
    public:
        /*
         * Get package list from url which contains json file.
         * Returns json document structure (rapidjson)
         */
        static Result<rapidjson::Document*> getPackageList(const char *url);
        
        /*
         * Return the package data for the package we can use
         * TODO: If we fail to fetch package from first url, try other other ones in the list (or if the first url is too slow / takes too long to respond).
         */
        static Result<PackageMetadata> getPackage(const char *packageName, const PackageVersionRange &versionRange, Platform platform);
    };
}

#endif //SIBS_PACKAGE_HPP
