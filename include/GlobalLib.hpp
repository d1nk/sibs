#ifndef SIBS_GLOBALLIB_HPP
#define SIBS_GLOBALLIB_HPP

#include "Result.hpp"
#include "Linker.hpp"
#include "Conf.hpp"

namespace sibs
{
    class PackageListDependency;
    
    class GlobalLib
    {
    public:
        enum DependencyError
        {
            DEPENDENCY_NOT_FOUND = 10,
            DEPENDENCY_VERSION_NO_MATCH = 20
        };

        static Result<bool> getLibs(const std::vector<PackageListDependency> &libs, const SibsConfig &parentConfig, const FileString &globalLibRootDir, LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc, LinkerFlagCallbackFunc dynamicLinkerFlagCallbackFunc, GlobalIncludeDirCallbackFunc globalIncludeDirCallback);
        static Result<bool> validatePackageExists(const FileString &globalLibRootDir, const std::string &name);
        static Result<bool> getLibsLinkerFlags(const SibsConfig &parentConfig, const FileString &globalLibRootDir, const std::string &name, const PackageVersionRange &versionRange, LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc, LinkerFlagCallbackFunc dynamicLinkerFlagCallbackFunc, GlobalIncludeDirCallbackFunc globalIncludeDirCallback);
        static Result<bool> downloadDependency(const PackageListDependency &dependency, Platform platform);
    private:
        static Result<bool> getLibsLinkerFlagsCommon(const SibsConfig &parentConfig, const FileString &packageDir, const std::string &dependencyName, LinkerFlagCallbackFunc staticLinkerFlagCallbackFunc, LinkerFlagCallbackFunc dynamicLinkerFlagCallbackFunc, GlobalIncludeDirCallbackFunc globalIncludeDirCallback);
    };
}

#endif //SIBS_GLOBALLIB_HPP
