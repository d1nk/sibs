#ifndef SIBS_ZLIB_HPP
#define SIBS_ZLIB_HPP

#include "Result.hpp"
#include "FileUtil.hpp"

namespace sibs
{
    class Archive
    {
    public:
        // Note: renames root directory in archive to @destination
        static Result<bool> extract(const _tinydir_char_t *source, const _tinydir_char_t *destination);
    };
}

#endif //SIBS_ZLIB_HPP
