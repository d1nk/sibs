#pragma once

#include <string>

namespace sibs
{
    enum class VersionOperation
    {
        NONE,
        LESS,
        LESS_EQUAL,
        EQUAL,
        GREATER,
        GREATER_EQUAL
    };
    
    const char* asString(VersionOperation operation);

    struct PackageVersion
    {
        int major;
        int minor;
        int patch;

        bool operator < (const PackageVersion &other) const
        {
            if(major < other.major) return true;
            if(major == other.major && minor < other.minor) return true;
            if(major == other.major && minor == other.minor && patch < other.patch) return true;
            return false;
        }

        bool operator == (const PackageVersion &other) const
        {
            return (major == other.major) && (minor == other.minor) && (patch == other.patch);
        }

        bool operator <= (const PackageVersion &other) const
        {
            return *this < other || *this == other;
        }

        std::string toString() const;
    };
    static_assert(sizeof(PackageVersion) == sizeof(int) * 3, "Expected PackageVersion to be the same size as 3 ints");

    struct PackageVersionRange
    {
        PackageVersionRange()
        {
            start = { 0, 0, 0 };
            end = { 0, 0, 0 };
            startDefined = false;
            endDefined = false;
            startOperation = VersionOperation::LESS;
            endOperation = VersionOperation::LESS;
        }

        bool isInRange(const PackageVersion &version) const;
        std::string toString() const;

        PackageVersion start;
        PackageVersion end;
        bool startDefined;
        bool endDefined;
        VersionOperation startOperation;
        VersionOperation endOperation;
    };
}