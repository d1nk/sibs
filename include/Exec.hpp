#ifndef SIBS_EXEC_HPP
#define SIBS_EXEC_HPP

#include "Result.hpp"
#include "../include/FileUtil.hpp"
#include <string>
#include <vector>

namespace sibs
{
    struct ExecResult
    {
        std::string execStdout;
        int exitCode;
    };

    Result<ExecResult> exec(const std::vector<FileString> &args, bool print_instead_of_pipe = false);
}

#endif //SIBS_EXEC_HPP
