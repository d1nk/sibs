#ifndef SIBS_RESULT_HPP
#define SIBS_RESULT_HPP

#include <cassert>
#include <string>

namespace sibs
{
	template <typename T>
	class Result
	{
	public:
		static Result Ok(const T &value)
		{
			Result result(value);
			result.errorCode = 0;
			return result;
		}
		
		static Result Ok(const T &&value)
		{
			Result result(value);
			result.errorCode = 0;
			return result;
		}

        template <typename OtherType>
        static Result Err(const Result<OtherType> &other)
        {
            Result result;
            result.errMsg = other.getErrMsg();
            result.errorCode = other.getErrorCode();
            return result;
        }

		static Result Err(const std::string &errMsg, int errorCode = 1)
		{
			Result result;
			result.errMsg = errMsg;
			result.errorCode = errorCode;
			return result;
		}

		bool isOk() const { return !errorCode; }
		bool isErr() const { return errorCode; }

		T& unwrap()
		{
			assert(isOk());
			return value;
		}

		const std::string &getErrMsg() const
		{
			assert(isErr());
			return errMsg;
		}

        int getErrorCode() const
        {
            return errorCode;
        }

		operator bool () { return isOk(); }
	private:
		Result(const T &_value = T()) : value(_value) {}
		Result(const T &&_value) : value(_value) {}
	private:
		T value;
		std::string errMsg;
		int errorCode;
	};
}

#endif //SIBS_RESULT_HPP
