#ifndef SIBS_UTILS_HPP
#define SIBS_UTILS_HPP

// Disable copying for a class or struct
#define DISABLE_COPY(ClassName) \
	ClassName(ClassName&) = delete; \
	ClassName& operator = (ClassName&) = delete;

#endif // SIBS_UTILS_HPP
