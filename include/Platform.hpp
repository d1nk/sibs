#pragma once

#include "types.hpp"
#include "StringView.hpp"
#include <vector>
#include <string>

namespace sibs
{
    enum Platform : u64
    {
        PLATFORM_INVALID    = 0x0000000000000000,

        PLATFORM_ANY        = 0xFFFFFFFFFFFFFFFF,

        PLATFORM_POSIX     = 1ULL << 1ULL,
        PLATFORM_POSIX32   = 1ULL << 2ULL | PLATFORM_POSIX,
        PLATFORM_POSIX64   = 1ULL << 3ULL | PLATFORM_POSIX,

        PLATFORM_LINUX      = 1ULL << 4ULL | PLATFORM_POSIX,
        PLATFORM_LINUX32    = 1ULL << 5ULL | PLATFORM_LINUX | PLATFORM_POSIX32,
        PLATFORM_LINUX64    = 1ULL << 6ULL | PLATFORM_LINUX | PLATFORM_POSIX64,

        PLATFORM_WIN        = 1ULL << 7ULL,
        PLATFORM_WIN32      = 1ULL << 8ULL | PLATFORM_WIN,
        PLATFORM_WIN64      = 1ULL << 9ULL | PLATFORM_WIN,

        PLATFORM_MACOS      = 1ULL << 10ULL | PLATFORM_POSIX,
        PLATFORM_MACOS32    = 1ULL << 11ULL | PLATFORM_MACOS | PLATFORM_POSIX32,
        PLATFORM_MACOS64    = 1ULL << 12ULL | PLATFORM_MACOS | PLATFORM_POSIX64,

        PLATFORM_BSD        = 1ULL << 13ULL | PLATFORM_POSIX,
        PLATFORM_OPENBSD    = 1ULL << 14ULL | PLATFORM_BSD,
        PLATFORM_OPENBSD32  = 1ULL << 15ULL | PLATFORM_OPENBSD | PLATFORM_POSIX32,
        PLATFORM_OPENBSD64  = 1ULL << 16ULL | PLATFORM_OPENBSD | PLATFORM_POSIX64,

        PLATFORM_HAIKU      = 1ULL << 24ULL | PLATFORM_POSIX,
        PLATFORM_HAIKU32    = 1ULL << 25ULL | PLATFORM_HAIKU | PLATFORM_POSIX32,
        PLATFORM_HAIKU64    = 1ULL << 26ULL | PLATFORM_HAIKU | PLATFORM_POSIX64,

        PLATFORM_LINUX_ARM      = 1ULL << 27ULL | PLATFORM_LINUX,
        PLATFORM_LINUX_ARM32    = 1ULL << 28ULL | PLATFORM_LINUX32,
        PLATFORM_LINUX_ARM64    = 1ULL << 29ULL | PLATFORM_LINUX64,

        PLATFORM_LINUX_X86      = 1ULL << 30ULL | PLATFORM_LINUX32,
        PLATFORM_LINUX_X86_64   = 1ULL << 31ULL | PLATFORM_LINUX64
    };

    const StringViewMap<Platform> PLATFORM_BY_NAME = {
        { "any", PLATFORM_ANY },
        { "posix", PLATFORM_POSIX },
        { "posix32", PLATFORM_POSIX32 },
        { "posix64", PLATFORM_POSIX64 },
        { "linux", PLATFORM_LINUX },
        { "linux32", PLATFORM_LINUX32 },
        { "linux64", PLATFORM_LINUX64 },
        { "win", PLATFORM_WIN },
        { "win32", PLATFORM_WIN32 },
        { "win64", PLATFORM_WIN64 },
        { "macos", PLATFORM_MACOS },
        { "macos32", PLATFORM_MACOS32 },
        { "macos64", PLATFORM_MACOS64 },
        { "bsd", PLATFORM_BSD },
        { "openbsd", PLATFORM_OPENBSD },
        { "openbsd32", PLATFORM_OPENBSD32 },
        { "openbsd64", PLATFORM_OPENBSD64 },
        { "haiku", PLATFORM_HAIKU },
        { "haiku32", PLATFORM_HAIKU32 },
        { "haiku64", PLATFORM_HAIKU64 },
        { "linux_arm", PLATFORM_LINUX_ARM },
        { "linux_arm32", PLATFORM_LINUX_ARM32 },
        { "linux_arm64", PLATFORM_LINUX_ARM64 },
        { "linux_x86", PLATFORM_LINUX_X86 },
        { "linux_x86_64", PLATFORM_LINUX_X86_64 }
    };

    bool containsPlatform(const std::vector<Platform> &platforms, Platform platform);
    const char* asString(Platform platform);
    Platform getPlatformByName(StringView name);

    std::string getPlatformListFormatted();

    // Return true if both platforms are of same type, for example if platform @a is either win, win32, win64 and @b is either win, win32, win64
    bool isSamePlatformFamily(Platform a, Platform b);
    // Returns true if platform @base is a base platform for platform @platform, for example if @platform is win64 and @base is either win64 or win, then this function return true.
    // If for example @platform is win64 and @base is win32, then this function returns false.
    bool isBaseForPlatform(Platform base, Platform platform);
    Platform getPlatformGenericType(Platform platform);
}
