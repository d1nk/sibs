#ifndef SIBS_LINKER_HPP
#define SIBS_LINKER_HPP

#include <functional>

namespace sibs
{
    using LinkerFlagCallbackFunc = std::function<void(const std::string&)>;
    using GlobalIncludeDirCallbackFunc = std::function<void(const std::string&)>;
    using CflagsCallbackFunc = std::function<void(const std::string&)>;
}

#endif //SIBS_LINKER_HPP
