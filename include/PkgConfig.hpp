#ifndef SIBS_PKGCONFIG_HPP
#define SIBS_PKGCONFIG_HPP

#include "env.hpp"
#include "Result.hpp"
#include <string>
#include <vector>
#include "FileUtil.hpp"
#include "Version.hpp"

namespace sibs
{
    class PackageListDependency;
    
    struct PkgConfigFlags
    {
        std::string linkerFlags;
        std::string cflags;
    };
    
    class PkgConfig
    {
    public:
        static void setPkgConfigPath(const FileString &path);
        static Result<bool> validatePkgConfigPackageVersionExists(const PackageListDependency &dependency);
        static Result<bool> validatePackageExists(const std::string &name);
        static Result<PackageVersion> getPackageVersion(const std::string &name);
        static Result<std::string> getDynamicLibsLinkerFlags(const std::vector<PackageListDependency> &libs);
        static Result<std::string> getDynamicLibsCflags(const std::vector<PackageListDependency> &libs);
        static Result<PkgConfigFlags> getDynamicLibsFlags(const std::vector<PackageListDependency> &libs);
    };
}

#endif // SIBS_PKGCONFIG_HPP
